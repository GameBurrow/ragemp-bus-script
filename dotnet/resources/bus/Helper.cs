using System;
using System.Linq;
using System.Text.RegularExpressions;
using GTANetworkAPI;

namespace Buses
{
	public class Helper : Script
	{
		public static DateTime getWorldTime()
		{
			/* If zerver changes to another timezone away from UTC, this needs to be changed output that timezone.
				if server changes to use non-real time, then zome huge hocus-pocus is needed to be made */
 			return DateTime.Now.ToUniversalTime();
		}

		public static string getWorldTimeString()
		{
			/* Returns in format 2008-10-31T17:04:32 as it's one of the few formats that does not inclde timezone info
				and won't mess up the time frontend due to local timezone*/
 			return getWorldTime().ToString("s");
		}

		/* Used to identify a bus from their hashcodes. */
		public static bool isBus(Vehicle maybeBus) {
			bool confirmBus = false;
			if (maybeBus != null) {
				if (new Int32[6]{-2007026063, 1283517198, -2072933068, -713569950, -1098802077, 1941029835}.Contains((Int32)(VehicleHash)maybeBus.Model)) {
					confirmBus = true;
				}
			}
			return confirmBus;
		}

		public static bool isDriver(sbyte seatID) {
			if (seatID == 0) {
				return true;
			}

			return false;
		}

		public static bool isValidDestination(string destination)
		{
			Regex regex = new Regex("^[a-zA-Z0-9\\s[\\]]*$");
			if (regex.IsMatch(destination)) {
				return true;
			}

			return false;
		}

		public static bool doesUserHaveManagingAccess(Player player) {
			/*
				Check here to if user is either:
				- Admin team members
				- In LS server, part of LSGOV faction and above rank 7
				- Not sure about LC server

				Check readme for more info
			*/
			return true;
		}

		public static bool doesUserHaveBusDriverAccess(Player player) {
			/*
				In LS Server we need to check if the driver is part of LSGOV faction. Not sure how it is in LC server.
				Check readme for more info
			*/
			return true;
		}

	}
}