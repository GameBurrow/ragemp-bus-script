using System;
using System.Collections.Generic;
using System.IO;
using GTANetworkAPI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Buses {
	class Lines : Script {
		/* Used to add a new line to the Buses classes dynamic 'busLines' list, either from a player executed command or from loading the json savefile. */
		public int newLine(string lineLabel, string routeLabel, int lineID = -1, int routeID = -1, bool published = false) {
			bool lineFound = false;
			bool routeFound = false;

			foreach (line busLine in Globals.busLines) {
				if (busLine.label.Equals(lineLabel) || busLine.lineID == lineID) {
					lineFound = true;

					if (routeID != -1) {
						routeFound = busLine.newRoute(routeLabel, routeID, published);
					} else if (busLine.routes.Count > 0) {
						routeFound = busLine.newRoute(routeLabel, busLine.routes[busLine.routes.Count - 1].routeID + 1, published);
					} else {
						routeFound = busLine.newRoute(routeLabel, 0, published);
					}

					if (routeFound == false) {
						this.storeLines();

						// Return 1 if new route added
						return 1;
					}
				}
			}
			if (!lineFound) {
				line newLine;
				if (lineID != -1) {
					newLine = new line(lineLabel, lineID);
				} else if (Globals.busLines.Count > 0) {
					newLine = new line(lineLabel, Globals.busLines[Globals.busLines.Count - 1].lineID + 1);
				} else {
					newLine = new line(lineLabel, 0);
				}
				newLine.newRoute(routeLabel, ( (routeID != -1) ? routeID : 0 ), published);
				Globals.busLines.Add(newLine);
				this.storeLines();

				// Return 1 if new line added
				return 1;

			}

			//Return 2 if line and route have been found (and we were adding route), meaning they exists
			if (lineFound && routeFound) {
				return 2;
			//Return 2 if line has been found and we were adding line, meaning they exists
			} else if (lineFound && !routeFound && lineLabel != null) {
				return 2;
			}
			return 0;
		}

		/* Edit line label*/
		public int editLine(int lineID, string lineLabel){
			bool lineLabelExists = false;

			int index = 0;
			while (index < Globals.busLines.Count) {
				if (Globals.busLines[index].label.Equals(lineLabel)) {
					lineLabelExists = true;
					break;
				}
				index++;
			}

			if (lineLabelExists) {
				return 2;
			}

			bool lineExists = false;
			int index2 = 0;
			while (index2 < Globals.busLines.Count) {
				if (Globals.busLines[index2].lineID.Equals(lineID)) {
					line editingLine = Globals.busLines[index2];
					Globals.busLines[index2] = new line(lineLabel, Globals.busLines[index2].lineID);
					Globals.busLines[index2].routes.AddRange(editingLine.routes);

					this.storeLines();
					lineExists = true;
					break;
				}
				index2++;
			}

			if (lineExists) {
				return 1;
			}
			return 0;
		}

		/* Used to delete a line from the Buses classes 'busLines' dynamic list, only activated via player executed command. */
		public bool delLine(int lineID) {
			int index = 0;
			while (index < Globals.busLines.Count) {
				if (Globals.busLines[index].lineID.Equals(lineID)) {
					Globals.busLines.RemoveAt(index);
					this.storeLines();
					return true;
				}
				index++;
			}

			return false;
		}

		public int editRoute(int lineID, int routeID, string routeLabel, string stopListJson) {
			var newStops = JsonConvert.DeserializeObject<List<lineStop>>(stopListJson);

			int lineIndex = 0;

			while (lineIndex < Globals.busLines.Count) {
				if (Globals.busLines[lineIndex].lineID.Equals(lineID)) {
					// Check if route with this label exists but it's not this route
					foreach (lineRoute route in Globals.busLines[lineIndex].routes) {
						if (route.version.Equals(routeLabel) && !route.routeID.Equals(routeID)) {
							// Cannot edit route because the new label already exists
							return 2;
						}
					}

					int routeIndex = 0;
					while (routeIndex < Globals.busLines[lineIndex].routes.Count) {
						if (Globals.busLines[lineIndex].routes[routeIndex].routeID.Equals(routeID)) {
							lineRoute editingRoute = Globals.busLines[lineIndex].routes[routeIndex];
							Globals.busLines[lineIndex].routes[routeIndex] = new lineRoute(routeLabel, routeID, editingRoute.published);
							Globals.busLines[lineIndex].routes[routeIndex].stopList.AddRange(newStops);

							this.storeLines();

							//Route edit successful
							return 1;

						}
						routeIndex++;
					}
				}
				lineIndex++;
			}

			return 0;
		}

		public bool toggleRoute(int lineID, int routeID) {
			int lineIndex = 0;

			while (lineIndex < Globals.busLines.Count) {
				if (Globals.busLines[lineIndex].lineID.Equals(lineID)) {
					int routeIndex = 0;
					while (routeIndex < Globals.busLines[lineIndex].routes.Count) {
						if (Globals.busLines[lineIndex].routes[routeIndex].routeID.Equals(routeID)) {
							lineRoute editingRoute = Globals.busLines[lineIndex].routes[routeIndex];

							bool published;

							if (editingRoute.published == true) {
								published = false;
							} else {
								published = true;
							}

							Globals.busLines[lineIndex].routes[routeIndex] = new lineRoute(editingRoute.version, editingRoute.routeID, published);
							Globals.busLines[lineIndex].routes[routeIndex].stopList.AddRange(editingRoute.stopList);

							this.storeLines();

							//Route edit successful
							return true;

						}
						routeIndex++;
					}
				}
				lineIndex++;
			}

			return false;
		}

		/* Used to delete a route from a line in the Buses classes 'busLines' dynamic list, only activated via player executed command. */
		public bool delRoute(int lineID, int routeID) {
			foreach (line busLine in Globals.busLines) {
				if (busLine.lineID.Equals(lineID)) {
					int routeIndex = 0;
					while (routeIndex < busLine.routes.Count) {
						if (busLine.routes[routeIndex].routeID.Equals(routeID)) {
							busLine.routes.RemoveAt(routeIndex);
							this.storeLines();

							// Route deleted
							return true;
						}
						routeIndex += 1;
					}
					break;
				}
			}

			return false;
		}

		/* Used to add an abstract stop to the Buses classes busLines dynamic lists 'line' structs 'routes' dynamic lists
		 'lineRoute' structs 'lineStops' dynamic list. */
		public int newRouteStop(int lineID, int routeID, Vector3? playerPos = null, int stopID = -1, int phyStopID = -1, int depOffset = 0) {
			int closestID = -1;
			int Index = 0;

			if (playerPos != null) {
				float greatDistance = 20;
				while (Index < Globals.busStops.Count) {
					var distance = Vector3.Distance(new Vector3(Globals.busStops[Index].pX, Globals.busStops[Index].pY, Globals.busStops[Index].pZ), (Vector3)playerPos);
					if (distance < greatDistance) {
						greatDistance = distance;
						closestID = Index;
					}
					Index += 1;
				}
			} else {
				while (Index < Globals.busStops.Count) {
					if (phyStopID == Globals.busStops[Index].ID) {
						phyStopID = Index;
						break;
					}
					Index += 1;
				}
			}

			int queryID = (phyStopID != -1) ? phyStopID : closestID;
			if (queryID != -1) {
				foreach (line busLine in Globals.busLines) {
					if (busLine.lineID.Equals(lineID)) {
						foreach (lineRoute route in busLine.routes) {
							if (route.routeID.Equals(routeID)) {
								Globals.busStops[queryID].relatedStops.Add(new stopNode(stopID, route.routeID, busLine.lineID));
								route.stopList.Add(new lineStop(stopID, Globals.busStops[queryID].ID, depOffset));
								this.storeLines();

								// Stop added to route
								return 1;
							}
						}
						break;
					}
				}
			} else {
				return 2;
			}

			return 0;
		}

		/* This will remove an abstract stop structs from the lines scope. */
		public int delRouteStop(int lineID, int routeID, int stopID) {
			bool lineFound = false;
			bool routeFound = false;
			int lineIndex = 0;

			while (lineIndex < Globals.busLines.Count) {
				if (Globals.busLines[lineIndex].lineID.Equals(lineID)) {
					lineFound = true;
					int routeIndex = 0;
					while (routeIndex < Globals.busLines[lineIndex].routes.Count) {
						if (Globals.busLines[lineIndex].routes[routeIndex].routeID.Equals(routeID)) {
							Int16 stopIndex = 0;
							while (stopIndex < Globals.busLines[lineIndex].routes[routeIndex].stopList.Count) {
								if (Globals.busLines[lineIndex].routes[routeIndex].stopList[stopIndex].stopID.Equals(stopID)) {
									Globals.busLines[lineIndex].routes[routeIndex].stopList.RemoveAt(stopIndex);
									this.storeLines();

									return 1;
								}
								stopIndex++;
							}
						}
						routeIndex++;
					}
				}
				lineIndex++;
			}

			if (!lineFound) {
				return 1;
			} else if (!routeFound) {
				return 2;
			}

			return 0;
		}

		/* This function is used to load lines from a json savefile and add them to the Buses classes 'busLines' dynamic list. */
		public void loadLines(){
			if (File.Exists(Globals.resourceFolder + "/json/lines.json")) {
				StreamReader file = new StreamReader(Globals.resourceFolder + "/json/lines.json");
				string fileData = file.ReadToEnd();
				file.Close();
				if (!fileData.Equals("")) {
					JObject jsonData = JObject.Parse(fileData);
					foreach (JObject busLine in jsonData.GetValue("lines")) {
						foreach (JObject route in busLine.GetValue("routes")) {
							this.newLine((String) busLine.GetValue("label"), (String) route.GetValue("version"), (Int32) busLine.GetValue("lineID"), (Int32) route.GetValue("routeID"), (bool) route.GetValue("published"));

							foreach (JObject stop in route.GetValue("stopList")) {
								this.newRouteStop((Int32) busLine.GetValue("lineID"), (Int32) route.GetValue("routeID"), null, (Int32) stop.GetValue("stopID"), (Int32) stop.GetValue("stopRef"), (Int32) stop.GetValue("offset"));
								if (Globals.nextLogicStopId <= (Int32) stop.GetValue("stopID")) {
									Globals.nextLogicStopId = (Int32) stop.GetValue("stopID") + 1;
								}
							}
						}

					}
				}
			}
		}

		/* This is used to unpack all the lines from the Buses classes dynamic list 'busLines', and store them in to  json file. */
		private void storeLines() {
			string dataInJson = JsonConvert.SerializeObject(Globals.busLines);
			FileStream fs = null;
			try {
				fs = new FileStream(Globals.resourceFolder + "/json/lines.json", FileMode.Create);
				using (StreamWriter writer = new StreamWriter(fs)) {
					writer.Write("{\"lines\":\n");
					writer.Write(dataInJson);
					writer.Write("\n}");
					writer.Close();
				}
			} finally {
				if (fs != null) {
					fs.Dispose();
				}
			}
		}
	}
}