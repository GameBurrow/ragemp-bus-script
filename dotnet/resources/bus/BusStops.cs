using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GTANetworkAPI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Buses {
	class BusStops : Script {
		/* With the directive Command, you can define how the server will function when it receives a request from a client
		with their command and arguments. This will add a new stop via createBusStop method. */
		[Command("stopadd", GreedyArg = true)]
		public void AddStop(Player player, string stopLabel) {
			if (!Helper.doesUserHaveManagingAccess(player)){
				NAPI.Chat.SendChatMessageToPlayer(player, "~r~You do not have access to this command.");
				return;
			}

			this.createBusStop(NAPI.Entity.GetEntityPosition(player) - new Vector3(0, 0, 1f),
							   NAPI.Entity.GetEntityRotation(player) - new Vector3(0, 0, 90), stopLabel, Globals.nextStopID, true);
			Globals.nextStopID += 1;
		}
		/* This function will change the bus stop structs name. */
		[Command("stopedit", GreedyArg = true)]
		public void editStop(Player player, string stopLabel)
		{
			if (!Helper.doesUserHaveManagingAccess(player)){
				NAPI.Chat.SendChatMessageToPlayer(player, "~r~You do not have access to this command.");
				return;
			}

			int nearestStop = -1;
			float nearestLabelDistance = 20;
			int iter = 0;
			while (iter < Globals.busStops.Count)
			{
				float distance = Vector3.Distance(new Vector3(Globals.busStops[iter].pX, Globals.busStops[iter].pY,
															Globals.busStops[iter].pZ), NAPI.Entity.GetEntityPosition(player));

				if (distance < nearestLabelDistance) { nearestLabelDistance = distance; nearestStop = iter; }
				iter += 1;
			}

			if (nearestStop != -1)
			{

				Globals.busStops[nearestStop] = new stop(Globals.busStops[nearestStop].ID, Globals.busStops[nearestStop].type, Globals.busStops[nearestStop].labelID, Globals.busStops[nearestStop].signID, stopLabel, Globals.busStops[nearestStop].pX, Globals.busStops[nearestStop].pY, Globals.busStops[nearestStop].pZ, Globals.busStops[nearestStop].rX, Globals.busStops[nearestStop].rY, Globals.busStops[nearestStop].rZ);
				this.storeStops();

				foreach (GTANetworkAPI.TextLabel label in NAPI.Pools.GetAllTextLabels())
				{
					if (label.Value == Globals.busStops[nearestStop].labelID)
					{
						NAPI.TextLabel.SetTextLabelText(label, stopLabel + "\n~m~/viewstop");
						break;
					}
				}
			}
		}

		/* This function with the assistance from 'moveStopConfirm' and 'moveStopCancel' will give the player possibility to change
		the stops position to the players current xyz */
		[Command("stopmove")]
		public void moveStop(Player player)
		{
			if (!Helper.doesUserHaveManagingAccess(player)){
				NAPI.Chat.SendChatMessageToPlayer(player, "~r~You do not have access to this command.");
				return;
			}

			// Find the nearest stop
			int nearestStop = -1;
			float nearestStopDistance = 20;
			int iter = 0;
			while (iter < Globals.busStops.Count)
			{
				float distance = Vector3.Distance(new Vector3(Globals.busStops[iter].pX, Globals.busStops[iter].pY, Globals.busStops[iter].pZ), NAPI.Entity.GetEntityPosition(player));

				if (distance < nearestStopDistance) {
					nearestStopDistance = distance;
					nearestStop = iter;
				}
				iter += 1;
			}

			if (nearestStop != -1)
			{
				//Check if player is already moving any stops
				foreach (movableStop stop in Globals.movableStops) {
					if (stop.player.Equals(player)) {
						NAPI.Chat.SendChatMessageToPlayer(player, "~r~You already moving a stop, if you want to cancel moving it, use /stopmovecancel.");
						return;
					} else if (stop.stopIndex.Equals(nearestStop)) {
						NAPI.Chat.SendChatMessageToPlayer(player, "~r~This stop is already being moved by somebody!");
						return;
					}
				}

				Globals.movableStops.Add(new movableStop(nearestStop, player));

				// moveableStop = nearestStop;
				NAPI.Chat.SendChatMessageToPlayer(player, "~g~You have selected stop: " +Globals.busStops[nearestStop].label+ " for moving. You have 5 minutes to move it.");
				NAPI.Chat.SendChatMessageToPlayer(player, "~g~Go to the new position and type /stopmovehere to move it or /stopmovecancel to cancel");

				// Schedule to cancel move automatically if not moved in 5 minutes
				System.Threading.Tasks.Task.Run(() => {
					NAPI.Task.Run(() =>{
						moveStopAutoAbort(player);
					}, delayTime: 300000);
				});
		}
			else
			{
				NAPI.Chat.SendChatMessageToPlayer(player, "~r~There are no stops near you.");
			}
		}

		[Command("stopmovehere")]
		public void moveStopConfirm(Player player)
		{
			if (!Helper.doesUserHaveManagingAccess(player)){
				NAPI.Chat.SendChatMessageToPlayer(player, "~r~You do not have access to this command.");
				return;
			}

			int index = 0;

			while (index < Globals.movableStops.Count) {
				if (Globals.movableStops[index].player.Equals(player)) {
					Vector3 position = NAPI.Entity.GetEntityPosition(player) - new Vector3(0, 0, 1f);
					Vector3 rotation = NAPI.Entity.GetEntityRotation(player) - new Vector3(0, 0, 90);

					int movableStopIndex = Globals.movableStops[index].stopIndex;
					stop oldStop = Globals.busStops[movableStopIndex];

					Globals.busStops[movableStopIndex] = new stop(Globals.busStops[movableStopIndex].ID, Globals.busStops[movableStopIndex].type, Globals.busStops[movableStopIndex].labelID, Globals.busStops[movableStopIndex].signID, Globals.busStops[movableStopIndex].label, position.X, position.Y, position.Z, rotation.X, rotation.Y, rotation.Z);
					Globals.busStops[movableStopIndex].relatedStops.AddRange(oldStop.relatedStops);

					this.storeStops();

					foreach (GTANetworkAPI.Object obj in NAPI.Pools.GetAllObjects()) {
						if (obj.Value == Globals.busStops[movableStopIndex].signID)
						{
							NAPI.Entity.SetEntityPosition(obj, position);
							NAPI.Entity.SetEntityRotation(obj, rotation);
							break;
						}
					}
					foreach (GTANetworkAPI.TextLabel label in NAPI.Pools.GetAllTextLabels()) {
						if (label.Value == Globals.busStops[movableStopIndex].labelID) {
							NAPI.Entity.SetEntityPosition(label, position + new Vector3(0, 0, 3f));
							break;
						}
					}
					Globals.movableStops.RemoveAt(index);
					NAPI.Chat.SendChatMessageToPlayer(player, "~g~Stop has been moved");
					return;
				}
				index++;
			}

			NAPI.Chat.SendChatMessageToPlayer(player, "~r~You haven't selected a stop for moving! Go near the stop and type /stopmove");
		}

		[Command("stopmovecancel")]
		public void moveStopCancel(Player player)
		{
			if (!Helper.doesUserHaveManagingAccess(player)){
				NAPI.Chat.SendChatMessageToPlayer(player, "~r~You do not have access to this command.");
				return;
			}

			int index = 0;
			while (index < Globals.movableStops.Count) {
				if (Globals.movableStops[index].player.Equals(player)) {
					Globals.movableStops.RemoveAt(index);
					NAPI.Chat.SendChatMessageToPlayer(player, "~g~Stop move has been cancelled");
					return;
				}
				index++;
			}

			NAPI.Chat.SendChatMessageToPlayer(player, "~r~You haven't selected any stops to be moved");
		}

		public void moveStopAutoAbort(Player player)
		{
			int index = 0;
			while (index < Globals.movableStops.Count) {
				if (Globals.movableStops[index].player.Equals(player)) {
					Globals.movableStops.RemoveAt(index);
					NAPI.Chat.SendChatMessageToPlayer(player, "~g~Stop move has been cancelled");
					return;
				}
				index++;
			}
		}

		/* This function will give you the possibility to delete the stop closest to the player. If no bus stop is found near you, it will just
		return a failure message. */
		[Command("stopdelete")]
		public void deleteStop(Player player) {
			if (!Helper.doesUserHaveManagingAccess(player)){
				NAPI.Chat.SendChatMessageToPlayer(player, "~r~You do not have access to this command.");
				return;
			}

			int nearestStop = -1;
			float nearestLabelDistance = 20;
			int iter = 0;
			while (iter < Globals.busStops.Count) {
				float distance = Vector3.Distance(new Vector3(Globals.busStops[iter].pX, Globals.busStops[iter].pY,
															Globals.busStops[iter].pZ), NAPI.Entity.GetEntityPosition(player));

				if (distance < nearestLabelDistance) { nearestLabelDistance = distance; nearestStop = iter; }
				iter += 1;
			}

			if (nearestStop != -1) {
				if (Globals.busStops[nearestStop].relatedStops.Count == 0) {
					foreach (GTANetworkAPI.Object obj in NAPI.Pools.GetAllObjects()) {
						if (obj.Value == Globals.busStops[nearestStop].signID) { NAPI.Entity.DeleteEntity(obj); break; }
					}
					foreach (GTANetworkAPI.TextLabel label in NAPI.Pools.GetAllTextLabels()) {
						if (label.Value == Globals.busStops[nearestStop].labelID) { NAPI.Entity.DeleteEntity(label); break; }
					}
					Globals.busStops.RemoveAt(nearestStop);
					this.storeStops();
				} else {
					NAPI.Chat.SendChatMessageToPlayer(player, "~r~You can not delete stops which have routes going through them.");
				}
			}
		}

		/* Used to create a physical bus stop from the player executed command, or loading from the savefile. */
		private void createBusStop(Vector3 pos, Vector3 rot, string stopLabel, int ID, bool isNewStop = false) {
			// Create the stop sign and label.
			GTANetworkAPI.Object placedObject = NAPI.Object.CreateObject(NAPI.Util.GetHashKey("prop_bus_stop_sign"), pos, rot);
			TextLabel placedLabel = NAPI.TextLabel.CreateTextLabel(stopLabel +"\n~m~/viewstop", pos + new Vector3(0, 0, 3f), 25, 1, 0, new Color(0, 255, 0), false);

			// Place the stop sign and label details in to a 'stop' struct.
			stop newStop = new stop(ID, 0, placedObject.Value, placedLabel.Value, stopLabel, pos.X, pos.Y, pos.Z, rot.X, rot.Y, rot.Z);
			Globals.busStops.Add(newStop);

			// Check if a 'isNewStop' parameter is provided, and if it is, save the details in JSON.
			if (isNewStop) {
				this.storeStops();
			}
		}

		public void storeStops() {
			Core core = new Core();

			string dataInJson = JsonConvert.SerializeObject(Globals.busStops);
			FileStream fs = null;
			try {
				fs = new FileStream(Globals.resourceFolder + "/json/busstops.json", FileMode.Create);
				using (StreamWriter writer = new StreamWriter(fs)) {
					writer.Write("{\"stops\":\n");
					writer.Write(dataInJson);
					writer.Write("\n}");
					writer.Close();
				}
			} finally {
				if (fs != null) {
					fs.Dispose();
				}
			}
		}

				/* This function is used to reload all the physical stops from all the clients in the client-pool. Usually executed when
		reloading a script without disconnecting clients. */
		public void reloadStops() {
			this.unloadStops();
			if (File.Exists(Globals.resourceFolder + "/json/busstops.json")) {
				StreamReader file = new StreamReader(Globals.resourceFolder + "/json/busstops.json");
				string fileData = file.ReadToEnd();
				file.Close();
				if (!fileData.Equals("")) {
					JObject jsonData = JObject.Parse(fileData);
					foreach (JObject busStop in jsonData.GetValue("stops")) {
						this.createBusStop(new Vector3((float) busStop.GetValue("pX"), (float) busStop.GetValue("pY"), (float) busStop.GetValue("pZ")),
										   new Vector3((float) busStop.GetValue("rX"), (float) busStop.GetValue("rY"), (float) busStop.GetValue("rZ")),
										   (String) busStop.GetValue("label"), (Int32) busStop.GetValue("ID"));
						if ((Int32) busStop.GetValue("ID") >= Globals.nextStopID) {
							Globals.nextStopID = (Int32) busStop.GetValue("ID") + 1;
						}
					}
				}
			}
		}
		/* Removes all the physical stop objects from the clients in the client-pool. */
		public void unloadStops() {
			foreach(stop stop in Globals.busStops) {
				NAPI.Entity.DeleteEntity(NAPI.Pools.GetAllObjects().First(obj => stop.signID == obj.Value));
				NAPI.Entity.DeleteEntity(NAPI.Pools.GetAllTextLabels().First(obj => stop.labelID == obj.Value));
			}
		}
	}
}