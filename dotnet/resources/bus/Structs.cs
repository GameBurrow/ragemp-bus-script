using System;
using System.Collections.Generic;
using GTANetworkAPI;

namespace Buses {
	// A struct format used for writing to a json savefile.
	public struct stopSave {
		public int ID;
		public Int16 type;
		public int labelID, signID;
		public string label;
		public float pX, pY, pZ, rX, rY, rZ;

		public stopSave(int id, Int16 stopType, int lID, int sID, string stopLabel, float posX, float posY, float posZ, float rotX, float rotY, float rotZ) {
			this.ID = id;
			this.type = stopType;
			this.labelID = lID;
			this.signID = sID;
			this.label = stopLabel;
			this.pX = posX;
			this.pY = posY;
			this.pZ = posZ;
			this.rX = rotX;
			this.rY = rotY;
			this.rZ = rotZ;
		}
	}

	/* A struct format used for runtime access to stop information, which is either loaded on runtime,
	loaded pre-run from savefile or off-loaded to stopSave struct for saving. */
	public struct stop {
		public int ID;
		public Int16 type;
		public int labelID, signID;
		public string label;
		public float pX, pY, pZ, rX, rY, rZ;
		public List<stopNode> relatedStops;

		public stop(int id, Int16 stopType, int lID, int sID, string stopLabel, float posX, float posY, float posZ, float rotX, float rotY, float rotZ) {
			this.ID = id;
			this.type = stopType;
			this.labelID = lID;
			this.signID = sID;
			this.label = stopLabel;
			this.pX = posX;
			this.pY = posY;
			this.pZ = posZ;
			this.rX = rotX;
			this.rY = rotY;
			this.rZ = rotZ;
			this.relatedStops = new List<stopNode>();
		}

		public stopSave getSave() {
			return new stopSave(this.ID, this.type, this.labelID, this.signID, this.label, this.pX, this.pY, this.pZ, this.rX, this.rY, this.rZ);
		}
	}

	/* Node used for connecting routes abstract stops and substantial stops. Syntaxtially better solution classes
	and inheritance between them, but optimized solution for a live server ends up being structs.*/
	public struct stopNode {
		public int stopID;
		public int routeID;
		public int lineID;

		public stopNode(int sID, int rID, int lID) {
			this.stopID = sID;
			this.routeID = rID;
			this.lineID = lID;
		}
	}

	/* A struct used for keeping the substantial stops in a lighter and abstract form, rather
	than directly accessing the substantial stop struct. */
	public struct lineStop {
		public int stopID;
		public int stopRef;
		public int offset;

		public lineStop(int id, int stop, int depOffset = 0) {
			this.stopID = id;
			this.stopRef = stop;
			this.offset = depOffset;
		}
	}

	/* A struct used for storing the abstract route which consists only within a line struct. */
	public struct lineRoute {
		public int routeID;
		public string version;
		public bool published;
		public List<lineStop> stopList;

		public lineRoute(string ver, int ID, bool published = true){
			this.routeID = ID;
			this.version = ver;
			this.published = published;
			this.stopList = new List<lineStop>();
		}

	}
	/* A line struct, which is stored within Buses classes dynamic busLines list. */
	public struct line {
		public int lineID;
		public string label;  // This is line number which would allow letters also like X1, S1 etc
		public List<lineRoute> routes;

		public line(string label, int ID) {
			this.lineID = ID;
			this.label = label;
			this.routes = new List<lineRoute>();
		}

		public bool newRoute(string route, int routeID, bool published = false) {
			bool routeExists = false;
			foreach (lineRoute listRoute in this.routes) {
				if (listRoute.version.Equals(route)) {
					routeExists = true;
				}
			}
			if (!routeExists) {
				this.routes.Add(new lineRoute(route, routeID, published));
			}
			return routeExists;
		}

		public bool appendStop(string route, lineStop stop) {
			bool routeNotFound = true;
			foreach (lineRoute listRoute in this.routes) {
				if (listRoute.version.Equals(route)) {
					listRoute.stopList.Add(stop);
					routeNotFound = false;
				}
			}
			return routeNotFound;
		}
	}
	/* A struct used for storing the current active vehicles on bus routes within Buses classes dynamic list 'onRoute' */
	public struct active {
		public int vehicleID;
		public int lineID;
		public int routeID;
		public int currentStopID;
		public int currentStopRef;
		public DateTime beginTime;
		public string operatingMode;

		public active(int vehicle, int lineID, int routeID, int currentStopID, int currentStopRef, DateTime begin, string operatingMode = "normal")
		{
			this.vehicleID = vehicle;
			this.lineID = lineID;
			this.routeID = routeID;
			this.currentStopID = currentStopID;
			this.currentStopRef = currentStopRef;
			this.beginTime = begin;
			this.operatingMode = operatingMode;
		}
	}

	// Struct used to temporarerly store on route vehicles with their posisions (needed by the GUI)
	public struct activeLocation {
		public int vehicleID;
		public float vehiclePosX;
		public float vehiclePosY;
		public float vehiclePosZ;
		public active activeData;

		public activeLocation(int vehicleID, float vehiclePosX, float vehiclePosY, float vehiclePosZ, active activeData)
		{
			this.vehicleID = vehicleID;
			this.vehiclePosX = vehiclePosX;
			this.vehiclePosY = vehiclePosY;
			this.vehiclePosZ = vehiclePosZ;
			this.activeData = activeData;
		}
	}

	public struct ibisLineList {
		public string lineLabel;
		public int lineID;
		public int routeID;
		public string destination;

		public ibisLineList(string lineLabel, int lineID, int routeID, string destination)
		{
			this.lineLabel = lineLabel;
			this.lineID = lineID;
			this.routeID = routeID;
			this.destination = destination;
		}
	}

	public struct vehicleStopList {
		public string stopName;
		public string stopTime;

		public bool currentStop;

		public vehicleStopList(string stopName, string stopTime, bool currentStop) {
			this.stopName = stopName;
			this.stopTime = stopTime;
			this.currentStop = currentStop;
		}
	}

	public struct movableStop {
		public int stopIndex;
		public Player player;

		public DateTime moveStarted;

		public movableStop(int stopIndex, Player player) {
			this.stopIndex = stopIndex;
			this.player = player;
			this.moveStarted = DateTime.Now;
		}
	}
}