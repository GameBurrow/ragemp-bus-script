
using System;
using System.Collections.Generic;
using GTANetworkAPI;
using Newtonsoft.Json;

namespace Buses {
	public class LineManager : Script {
		[Command("transitmanager",Alias="busmanager,lstmanager,linemanager")]
		public void openTransportManager(Player player) {
			if (!Helper.doesUserHaveManagingAccess(player)){
				NAPI.Chat.SendChatMessageToPlayer(player, "~r~You do not have access to this command.");
				return;
			}

			NAPI.ClientEvent.TriggerClientEvent(
				player,
				"openTransitManager",
				JsonConvert.SerializeObject(Globals.busStops),
				JsonConvert.SerializeObject(Globals.busLines)
			);
		}

		public void returnToTransitManager(Player player, string view = null, string responseType = null, string responseMessage = null, string extraData = null, string messageUpdateOnly = "no"){
			NAPI.ClientEvent.TriggerClientEvent(
				player,
				"openTransitManager",
				JsonConvert.SerializeObject(Globals.busStops),
				JsonConvert.SerializeObject(Globals.busLines),
				view,
				responseType,
				responseMessage,
				extraData,
				messageUpdateOnly
			);
		}

		[RemoteEvent("transitManagerAddLine")]
		public void transitManagerAddLine(Player player, string lineLabel) {
			if (!Helper.doesUserHaveManagingAccess(player)){
				NAPI.Chat.SendChatMessageToPlayer(player, "~r~You do not have access to this action.");
				return;
			}

			Lines lines = new Lines();
			int returns = lines.newLine(lineLabel, "A");

			if (returns == 1) {
				returnToTransitManager(player, "lineview", "success", "Line has been created");
			} else if (returns == 2) {
				returnToTransitManager(player, null, "error", "Line with this label already exists", null, "yes");
			} else {
				returnToTransitManager(player, null, "error", "Unknown error occured", null, "yes");
			}

		}

		[RemoteEvent("transitManagerEditLine")]
		public void transitManagerEditLine(Player player, string lineID, string lineLabel) {
			if (!Helper.doesUserHaveManagingAccess(player)){
				NAPI.Chat.SendChatMessageToPlayer(player, "~r~You do not have access to this action.");
				return;
			}

			Lines lines = new Lines();

			int lineIDint = -1;
			bool lineIDNumeric = int.TryParse(lineID, out lineIDint);

			if (!lineIDNumeric) {
				returnToTransitManager(player, null, "error", "Unable to edit line, malformed data", null, "yes");
				return;
			}

			int returns = lines.editLine(lineIDint, lineLabel);

			if (returns == 1){
				returnToTransitManager(player, "lineview", "success", "Line label has been changed");
			} else if (returns == 2) {
				returnToTransitManager(player, null, "error", "Line with this label already exists!", null, "yes");
			} else if (returns == 0) {
				returnToTransitManager(player, null, "error", "Line not found", null, "yes");
			} else {
				returnToTransitManager(player, null, "error", "Unkown error occured", null, "yes");
			}
		}

		[RemoteEvent("transitManagerDeleteLine")]
		public void transitManagerDeleteLine(Player player, string lineID) {
			if (!Helper.doesUserHaveManagingAccess(player)){
				NAPI.Chat.SendChatMessageToPlayer(player, "~r~You do not have access to this action.");
				return;
			}

			int lineIDint = -1;
			bool lineIDNumeric = int.TryParse(lineID, out lineIDint);

			if (!lineIDNumeric) {
				returnToTransitManager(player, null, "error", "Unable to delete line, malformed data", null, "yes");
				return;
			}

			Lines lines = new Lines();
			if(lines.delLine(lineIDint)) {
				returnToTransitManager(player, "lineview", "success", "Line deleted");
			} else {
				returnToTransitManager(player, null, "error", "Unable to delete line", null, "yes");
			}
		}

		[RemoteEvent("transitManagerAddRoute")]
		public void transitManagerAddRoute(Player player, string lineID, string lineLabel) {
			if (!Helper.doesUserHaveManagingAccess(player)){
				NAPI.Chat.SendChatMessageToPlayer(player, "~r~You do not have access to this action.");
				return;
			}

			int lineIDint = -1;
			bool lineIDNumeric = int.TryParse(lineID, out lineIDint);

			if (!lineIDNumeric) {
				returnToTransitManager(player, null, "error", "Unable to add route, malformed data", null, "yes");
				return;
			}

			Lines lines = new Lines();
			int returns = lines.newLine(null, lineLabel, lineIDint);

			if (returns == 1){
				returnToTransitManager(player, "lineview", "success", "Route has been added to the line");
			} else if (returns == 2) {
				returnToTransitManager(player, null, "error", "Route with this label already exists on this line!", null, "yes");
			} else if (returns == 0) {
				returnToTransitManager(player, null, "error", "Route not added. Error occured", null, "yes");
			} else {
				returnToTransitManager(player, null, "error", "Unkown error occured", null, "yes");
			}
		}

		[RemoteEvent("transitManagerEditRoute")]
		public void transitManagerEditRoute(Player player, string lineID, string routeID, string routeLabel, string stopList) {
			if (!Helper.doesUserHaveManagingAccess(player)){
				NAPI.Chat.SendChatMessageToPlayer(player, "~r~You do not have access to this action.");
				return;
			}

			int lineIDint = -1;
			bool lineIDNumeric = int.TryParse(lineID, out lineIDint);

			if (!lineIDNumeric) {
				returnToTransitManager(player, null, "error", "Unable to edit routee, malformed data", null, "yes");
				return;
			}

			int routeIDint = -1;
			bool routeIDNumeric = int.TryParse(routeID, out routeIDint);

			if (!routeIDNumeric) {
				returnToTransitManager(player, null, "error", "Unable to edit route, malformed data", null, "yes");
				return;
			}

			Lines lines = new Lines();
			int edited = lines.editRoute(lineIDint, routeIDint, routeLabel, stopList);

			var returnData = new Dictionary<string,string> {
				{"lineID", lineID},
				{"routeID", routeID},
			};

			if (edited == 1) {
				returnToTransitManager(player, "routeedit", "success", "Saved", JsonConvert.SerializeObject(returnData));
				return;
			} else if (edited == 2) {
				returnToTransitManager(player, null, "error", "Route label already exists on this line", JsonConvert.SerializeObject(returnData), "yes");
				return;
			}

			returnToTransitManager(player, null, "error", "Unable to edit route", JsonConvert.SerializeObject(returnData), "yes");
		}

		[RemoteEvent("transitManagerToggleRoute")]
		public void transitManagerToggleRoute(Player player, string lineID, string routeID) {
			if (!Helper.doesUserHaveManagingAccess(player)){
				NAPI.Chat.SendChatMessageToPlayer(player, "~r~You do not have access to this action.");
				return;
			}

			int lineIDint = -1;
			bool lineIDNumeric = int.TryParse(lineID, out lineIDint);

			if (!lineIDNumeric) {
				returnToTransitManager(player, null, "error", "Unable to edit routee, malformed data", null, "yes");
				return;
			}

			int routeIDint = -1;
			bool routeIDNumeric = int.TryParse(routeID, out routeIDint);

			if (!routeIDNumeric) {
				returnToTransitManager(player, null, "error", "Unable to edit route, malformed data", null, "yes");
				return;
			}

			Lines lines = new Lines();

			var returnData = new Dictionary<string,string> {
				{"lineID", lineID},
				{"routeID", routeID},
			};

			if (lines.toggleRoute(lineIDint, routeIDint)) {
				returnToTransitManager(player, "lineview", "success", "Route publish state changed", JsonConvert.SerializeObject(returnData));
				return;
			}

			returnToTransitManager(player, null, "error", "Unable to toggle route", JsonConvert.SerializeObject(returnData), "yes");
		}

		[RemoteEvent("transitManagerDeleteRoute")]
		public void transitManagerDeleteRoute(Player player, string lineID, string routeID) {
			if (!Helper.doesUserHaveManagingAccess(player)){
				NAPI.Chat.SendChatMessageToPlayer(player, "~r~You do not have access to this action.");
				return;
			}

			int lineIDint = -1;
			bool lineIDNumeric = int.TryParse(lineID, out lineIDint);

			if (!lineIDNumeric) {
				returnToTransitManager(player, null, "error", "Unable to delete route, malformed data", null, "yes");
				return;
			}

			int routeIDint = -1;
			bool routeIDNumeric = int.TryParse(routeID, out routeIDint);

			if (!routeIDNumeric) {
				returnToTransitManager(player, null, "error", "Unable to delete route, malformed data", null, "yes");
				return;
			}

			Lines lines = new Lines();

			if (lines.delRoute(lineIDint, routeIDint)) {
				returnToTransitManager(player, "lineview", "success", "Route deleted");
				return;
			}

			returnToTransitManager(player, null, "error", "Unable to delete route!", null, "yes");
		}

		[RemoteEvent("transitManagerAddRouteStopNearest")]
		public void transitManagerAddRouteStopNearest(Player player, string lineID, string routeID){
			if (!Helper.doesUserHaveManagingAccess(player)){
				NAPI.Chat.SendChatMessageToPlayer(player, "~r~You do not have access to this action.");
				return;
			}

			int lineIDint = -1;
			bool lineIDNumeric = int.TryParse(lineID, out lineIDint);

			if (!lineIDNumeric) {
				returnToTransitManager(player, null, "error", "Unable to add nearest stop, malformed data", null, "yes");
				return;
			}

			int routeIDint = -1;
			bool routeIDNumeric = int.TryParse(routeID, out routeIDint);

			if (!routeIDNumeric) {
				returnToTransitManager(player, null, "error", "Unable to add nearest stop, malformed data", null, "yes");
				return;
			}

			Lines lines = new Lines();
			int returns = lines.newRouteStop(lineIDint, routeIDint, NAPI.Entity.GetEntityPosition(player), Globals.nextLogicStopId);
			Globals.nextLogicStopId += 1;

			var returnData = new Dictionary<string,string> {
				{"lineID", lineID},
				{"routeID", routeID},
			};

			if (returns == 1) {
				returnToTransitManager(player, "routeedit", "success", "Stop added to route", JsonConvert.SerializeObject(returnData));
				return;
			} else if (returns == 2) {
				returnToTransitManager(player, null, "error", "No stop has been located near you, use /stopadd to create a new one!", null, "yes");
				return;
			} else if (returns == 0) {
				returnToTransitManager(player, null, "error", "Unable to add stop to the route", null, "yes");
				return;
			}

			returnToTransitManager(player, null, "error", "Unknown error occured", null, "yes");
		}

		[RemoteEvent("transitManagerDeleteRouteStop")]
		public void transitManagerDeleteRouteStop(Player player, string lineID, string routeID, string stopID) {
			if (!Helper.doesUserHaveManagingAccess(player)){
				NAPI.Chat.SendChatMessageToPlayer(player, "~r~You do not have access to this action.");
				return;
			}

			int lineIDint = -1;
			bool lineIDNumeric = int.TryParse(lineID, out lineIDint);

			if (!lineIDNumeric) {
				returnToTransitManager(player, null, "error", "Unable to remove nearest stop, malformed data", null, "yes");
				return;
			}

			int routeIDint = -1;
			bool routeIDNumeric = int.TryParse(routeID, out routeIDint);

			if (!routeIDNumeric) {
				returnToTransitManager(player, null, "error", "Unable to remove nearest stop, malformed data", null, "yes");
				return;
			}

			int stopIDint = -1;
			bool stopIDNumeric = int.TryParse(stopID, out stopIDint);

			if (!stopIDNumeric) {
				returnToTransitManager(player, null, "error", "Unable to remove nearest stop, malformed data", null, "yes");
				return;
			}

			Lines lines = new Lines();

			int returns = lines.delRouteStop(lineIDint, routeIDint, stopIDint);

			var returnData = new Dictionary<string,string> {
				{"lineID", lineID},
				{"routeID", routeID},
			};

			if (returns == 1) {
				returnToTransitManager(player, "routeedit", "success", "Stop removed", JsonConvert.SerializeObject(returnData));
				return;
			} else if (returns == 0 || returns == 2) {
				returnToTransitManager(player, "routeedit", "success", "Unable to remove stop from line", JsonConvert.SerializeObject(returnData));
				return;
			}

			returnToTransitManager(player, null, "error", "Unknown error occured", null, "yes");
		}
	}
}