/*
	Bus network script by GameBurrow & Cubi*
	Reccomended org flag: BUS

	Includes:
	- Dynamic bus stops
	- Dynamic lines
	- Driver checkpoints
	- Bus destination display system
	- Timetable system

	Commands
	-------------COMMAND USABALE BY EVERYBODY, EVEN NON-ORG MEMBERS------------------
	| - /transitapp - Open Transit app to view info
	| - /viewstop - Open Transit app with the nearest stop info (radius 10)
	---------------------------------------------------------------------------------

	-------------COMMANDs LIMITED TO ADMINS & ORG LEADERS, flag: BUS-MNG--------------
	| - /stopadd [name] - Creates a new bustop with object and label @ player location and facing the same way as player
	| - /stopmove - attempts to move nearest stop position
	| - - /stopmovehere - confirms the move
	| - - /stopmovecancel - cancels the stop moving procedure
	| - /stopedit [name] - updates the stop description and name
	| - /stopdelete - deletes the nearest stop to the player
	|
	| - /transitmanager - To add, edit, delete and (un-)publish lines
	----------------------------------------------------------------------------------
 */
using System;
using System.Text;
using GTANetworkAPI;

namespace Buses {
	/*
		Buses class, which inherits the Script class, causing it it function as a callable script if it has been linked
		to the rage:mp server via GTA:Network link.
	 */

	public class Core : Script {
		Encoding encoding = new UTF8Encoding(true, true);

		/* With ServerEvent, you can hook in to the servers runtime events, which allow you to eg. launch a function on the
		current resource initialization and destruction. */
		[ServerEvent(Event.ResourceStart)]
		public void myResourceStart() {
			Globals.resourceFolder = NAPI.Resource.GetResourceFolder(this);
			// This will be run once all scripts are initialized!
			NAPI.Util.ConsoleOutput("Starting bus system script!");

			// Check if json folder exists
			if (!System.IO.Directory.Exists(Globals.resourceFolder + "/json")){
 				System.IO.Directory.CreateDirectory(Globals.resourceFolder + "/json");
			}

			/*NAPI.Task.Run(() =>
			{
				this.reloadStops();
				this.loadLines();
			}, delayTime: 10000); // We delay the task invocation, needed because if player connects to the server immediately after restart, he won't see the first object*/
			BusStops busStopsClass = new BusStops();
			busStopsClass.reloadStops();

			Lines linesClass = new Lines();
			linesClass.loadLines();
		}

		[ServerEvent(Event.ResourceStop)]
		public void OnResourceStop()
		{
			BusStops busStopsClass = new BusStops();
			busStopsClass.unloadStops();
		}

		/* When a player enters a vehicle, check if the vehicle is currently active on a route,
		and if it is, it will show the player the necessary GUI. */
		[ServerEvent(Event.PlayerEnterVehicle)]
		public void OnPlayerEnterVehicle(Player player, Vehicle vehicle, sbyte seatID)
		{
			//If entered vehicle is a bus and if player is driver
			if (Helper.isBus(vehicle) && Helper.isDriver(seatID)) {
				// Prevent loading the Ibis HUD for players that shouldn't have access to it
				if (!Helper.doesUserHaveBusDriverAccess(player)){
					return;
				}
				// NAPI.Util.ConsoleOutput("Player entered vehicle");
				BusControl buscontrol = new BusControl();
				buscontrol.updateDriverHud(player, vehicle);
			}
		}

		/* If the vehicle is active on the route, and a player exits it,  all the displayed GUI's will be accordingly removed. */
		[ServerEvent(Event.PlayerExitVehicle)]
		public void OnPlayerExitVehicle(Player player, Vehicle vehicle)
		{
			//NAPI.Util.ConsoleOutput("Player exited vehicle");
			if (Helper.isBus(vehicle))
			{
				//NAPI.Chat.SendChatMessageToPlayer(player, "You left a bus with active schedule");
				NAPI.ClientEvent.TriggerClientEvent(player, "driverClearBlipAndCol");
			}
		}

		/* On player connection, make sure the client has all the necessary resources synced and downloaded. */
		[ServerEvent(Event.PlayerConnected)]
		public void OnPlayerConnected(Player player)
		{
			foreach(Vehicle vehicle in NAPI.Pools.GetAllVehicles())
			{
				if(vehicle.HasData("destination"))
				{
					foreach(Player player1 in NAPI.Pools.GetAllPlayers())
					{
						NAPI.ClientEvent.TriggerClientEvent(player1, "setBusDestination", vehicle, new Vector3(0, 0, 3.5), vehicle.GetData<string>("destination"));
					}
					// NAPI.ClientEvent.TriggerClientEventForAll("setBusDestination", vehicle, new Vector3(0, 0, 3.5), NAPI.Data.GetEntityData(vehicle, "destination")); // Doesn't work after the latest RageMP updates anymore
				}
			}
		}
	}
}