using System;
using System.Collections.Generic;

namespace Buses {
	static class Globals {
		public static string resourceFolder;
		public static List<active> onRoute = new List<active>();

		public static List<line> busLines = new List<line>();

		public static List<stop> busStops = new List<stop>();

		public static List<movableStop> movableStops = new List<movableStop>();
		public static int nextStopID = 0;
		public static int nextLogicStopId = 0;

	}
}