using System;
using System.Collections.Generic;
using GTANetworkAPI;
using Newtonsoft.Json;

namespace Buses {
	class TransitApp : Script {
		/* STOP INFO */

		/* This will open the transit app with the nearest stop view. */
		[Command("viewstop")]
		public void viewStop(Player player)
		{
			int nearestStop = -1;
			float nearestLabelDistance = 10;
			int iter = 0;
			while (iter < Globals.busStops.Count) {
				float distance = Vector3.Distance(new Vector3(Globals.busStops[iter].pX, Globals.busStops[iter].pY,
															Globals.busStops[iter].pZ), NAPI.Entity.GetEntityPosition(player));

				if (distance < nearestLabelDistance) { nearestLabelDistance = distance; nearestStop = iter; }
				iter += 1;
			}
			if (nearestStop != -1) {
				Vector3 playerPos = NAPI.Entity.GetEntityPosition(player);

				NAPI.ClientEvent.TriggerClientEvent(
					player,
					"openTransitAppStopView",
					JsonConvert.SerializeObject(Globals.busStops[nearestStop].ID),
					JsonConvert.SerializeObject(Globals.busStops),
					JsonConvert.SerializeObject(playerPos),
					JsonConvert.SerializeObject(Globals.busLines),
					getActiveBusPositions(),
					Helper.getWorldTimeString()
				);
			} else {
				NAPI.Chat.SendChatMessageToPlayer(player, "~r~You aren't near any working bus stops!");
			}
		}

		/* TRANSIT APP UI */

		[Command("transitapp", Alias="buses,busapp,lstapp")]
		public void transitapp(Player player)
		{
			Vector3 playerPos = NAPI.Entity.GetEntityPosition(player);

			NAPI.ClientEvent.TriggerClientEvent(
				player,
				"openTransitApp",
				JsonConvert.SerializeObject(Globals.busStops),
				JsonConvert.SerializeObject(playerPos),
				JsonConvert.SerializeObject(Globals.busLines),
				getActiveBusPositions(),
				Helper.getWorldTimeString()
			);
		}

		public string getActiveBusPositions()
		{
			List<activeLocation> onroutevehicles = new List<activeLocation>();
			foreach(Vehicle vehicle in NAPI.Pools.GetAllVehicles())
			{
				if(vehicle.HasData("ibisstatus") && vehicle.GetData<string>("ibisstatus") == "operation" && vehicle.HasData("busid"))
				{
					int vehicleID = vehicle.GetData<int>("busid");
					BusControl busControl = new BusControl();
					foreach (active activeBus in Globals.onRoute) {
						if (vehicleID == activeBus.vehicleID && activeBus.operatingMode == "normal") {
							Vector3 vehiclePos = NAPI.Entity.GetEntityPosition(vehicle);

							activeLocation activeBusWithlocation = new activeLocation(vehicleID, vehiclePos.X, vehiclePos.Y, vehiclePos.Z, activeBus);
							onroutevehicles.Add(activeBusWithlocation);
						}
					}
				}
			}

			return JsonConvert.SerializeObject(onroutevehicles);
		}

		[RemoteEvent("getLiveBuses")]
		public void getLiveBuses(Player player)
		{
			NAPI.ClientEvent.TriggerClientEvent(
				player,
				"updateLiveBuses",
				getActiveBusPositions()
			);
		}
	}
}