using System;
using System.Collections.Generic;
using System.Linq;
using GTANetworkAPI;
using Newtonsoft.Json;

namespace Buses {
	class BusControl : Script {
		private List<String> destinations = new List<String>() {
			"Not in Service", "Charter ride", "Training bus", "Test drive", "On break"
		};

		public void updateDriverHud(Player player, Vehicle vehicle)
		{
			stop busStop;

			int activeRouteDataIndex;

			string ibisstatus;

			string worldTime = Helper.getWorldTimeString();

			if (vehicle.HasData("ibisstatus")) {
				ibisstatus = vehicle.GetData<string>("ibisstatus");
			} else {
				ibisstatus = "off";
			}

			if (ibisstatus == "destination")
			{
				string destination = "Not in Service";
				if (vehicle.HasData("destination") && vehicle.GetData<string>("destination") != null)
				{
					destination = vehicle.GetData<string>("destination");
				}

				NAPI.ClientEvent.TriggerClientEvent(player, "drawHud", ibisstatus, null, null, null, destination, false, worldTime);
				return;
			}
			else if (ibisstatus == "operation")
			{
				if ((activeRouteDataIndex = Globals.onRoute.FindIndex(obj => obj.vehicleID == vehicle.Value)) != -1)
				{
					active activeRouteData = Globals.onRoute.ElementAt(activeRouteDataIndex);
					BusStops busStops = new BusStops();
					busStop = Globals.busStops.Find(obj => obj.ID == activeRouteData.currentStopRef);

					NAPI.ClientEvent.TriggerClientEvent(player, "drawHud", ibisstatus, busStop.pX, busStop.pY, busStop.pZ, JsonConvert.SerializeObject(getVehicleStopList(vehicle, true)), true, worldTime);
					return;
				}
			}

			NAPI.ClientEvent.TriggerClientEvent(player, "drawHud", ibisstatus, null, null, null, null, false, worldTime);
		}

		// This is called by CEF to turn IBIS system on or off
		[RemoteEvent("ibisToggle")]
		public void ibisToggle(Player player, string mode)
		{
			if (new String[2] { "off", "on" }.Contains(mode))
			{
				Vehicle playerVeh = NAPI.Player.GetPlayerVehicle(player);

				if (NAPI.Player.GetPlayerVehicleSeat(player) == 0 && Helper.isBus(playerVeh))
				{
					if (mode == "on")
					{
						playerVeh.SetData<string>("ibisstatus", "destination");
						playerVeh.SetData<int>("busid", playerVeh.Value);
						this.setBusTerminus(playerVeh, null, "Not in Service");

						NAPI.ClientEvent.TriggerClientEvent(player, "drawHud", "destination", null, null, null, playerVeh.GetData<string>("destination"), false);
					}
					else
					{
						playerVeh.ResetData("ibisstatus");
						playerVeh.ResetData("busid");
						NAPI.ClientEvent.TriggerClientEvent(player, "driverClearBlipAndCol", true);
						this.endLine(playerVeh);

						NAPI.ClientEvent.TriggerClientEvent(player, "drawHud", "off", null, null, null, null, false);
					}
				}
			}
		}

		[RemoteEvent("ibisOpenLineSelect")]
		public void ibisOpenLineSelect(Player player, string selectedOperatingMode)
		{
			List<ibisLineList> lines = getLinesForIbisSelect(selectedOperatingMode);

			NAPI.ClientEvent.TriggerClientEvent(player, "drawHud", "selectline", null, null, null, JsonConvert.SerializeObject(lines), false);
		}

		public List<ibisLineList> getLinesForIbisSelect(string selectedOperatingMode)
		{
			List<ibisLineList> lines = new List<ibisLineList>();

			foreach(line busLine in Globals.busLines)
			{
				foreach(lineRoute busRoute in busLine.routes)
				{
					if(
						busRoute.stopList.Count != 0
						&& (
							(selectedOperatingMode != "testing" && busRoute.published)
							|| selectedOperatingMode == "testing"
						)
					)
					{
						lines.Add(new ibisLineList(
							busLine.label, // lineLabel
							busLine.lineID, // lineID
							busRoute.routeID, // routeID
							Globals.busStops.First(obj => obj.ID == busRoute.stopList[busRoute.stopList.Count - 1].stopRef).label //destination
						));
					}
				}
			}

			return lines;
		}

		[RemoteEvent("ibisSelectLine")]
		public void ibisSelectLine (Player player, int lineID, int routeID, string departureTimeString, string selectedOperatingMode)
		{
			Vehicle playerVeh = NAPI.Player.GetPlayerVehicle(player);

			bool routeFound = false;
			bool lineFound = false;

			if ((Globals.onRoute.FindIndex(obj => obj.vehicleID == playerVeh.Value)) != -1) {
				NAPI.ClientEvent.TriggerClientEvent(player, "driverClearBlipAndCol", true);
				this.endLine(playerVeh, Globals.onRoute.FindIndex(obj => obj.vehicleID == playerVeh.Value));
			}

			foreach (line busLine in Globals.busLines) {

				if (busLine.lineID.Equals(lineID)) {

					lineFound = true;

					foreach (lineRoute route in busLine.routes) {

						if (route.routeID.Equals(routeID)) {
							routeFound = true;
							DateTime departureTime = DateTime.Parse(departureTimeString);

							active newDriver = new active(playerVeh.Value, busLine.lineID, route.routeID, route.stopList[0].stopID, route.stopList[0].stopRef, departureTime, selectedOperatingMode);
							stop stop = Globals.busStops.First(item => item.ID == route.stopList[0].stopRef);
							Globals.onRoute.Add(newDriver);

							if (selectedOperatingMode == "training") {
								this.setBusTerminus(playerVeh, null, "Training bus");
							} else if (selectedOperatingMode == "testing") {
								this.setBusTerminus(playerVeh, null, "Test Drive");
							} else {
								this.setBusTerminus(playerVeh, busLine.label, Globals.busStops.First(obj => obj.ID == route.stopList[route.stopList.Count - 1].stopRef).label);
							}

							// this.setBusTerminus(playerVeh, busLine.label, Globals.busStops.First(obj => obj.ID == route.stopList[route.stopList.Count - 1].stopRef).label);
							playerVeh.SetData<string>("ibisstatus", "operation");

							NAPI.ClientEvent.TriggerClientEvent(player, "drawHud", "operation", stop.pX, stop.pY, stop.pZ, JsonConvert.SerializeObject(getVehicleStopList(playerVeh, true)));

							break;
						}
					}
				}
			}
			if (lineFound == false || routeFound == false) {
				NAPI.Chat.SendChatMessageToPlayer(player, "~r~Selected route does not exist!");
			}

		}

		[RemoteEvent("ibisOpenDestSelect")]
		public void ibisOpenDestSelect(Player player)
		{
			NAPI.ClientEvent.TriggerClientEvent(player, "drawHud", "selectdest", null, null, null, JsonConvert.SerializeObject(this.destinations), false);
		}

		[RemoteEvent("ibisSelectDest")]
		public void ibisSelectDest (Player player, string destination)
		{
			if (!Helper.isValidDestination(destination)) {
				NAPI.Chat.SendChatMessageToPlayer(player, "~r~Invalid destination!");
				return;
			}

			Vehicle playerVeh = NAPI.Player.GetPlayerVehicle(player);

			if ((Globals.onRoute.FindIndex(obj => obj.vehicleID == playerVeh.Value)) != -1) {
				NAPI.ClientEvent.TriggerClientEvent(player, "driverClearBlipAndCol", true);
				this.endLine(playerVeh, Globals.onRoute.FindIndex(obj => obj.vehicleID == playerVeh.Value));
			}


			this.setBusTerminus(playerVeh, null, destination);
			playerVeh.SetData<string>("ibisstatus", "destination");

			NAPI.ClientEvent.TriggerClientEvent(player, "drawHud", "destination", null, null, null, destination, false);

		}

		[RemoteEvent("ibisClearLine")]
		public void ibisClearLine (Player player)
		{
			Vehicle playerVeh = NAPI.Player.GetPlayerVehicle(player);

			int activeRouteDataIndex;
			if ((activeRouteDataIndex = Globals.onRoute.FindIndex(obj => obj.vehicleID == playerVeh.Value)) != -1)
			{
				this.endLine(playerVeh, activeRouteDataIndex);
				playerVeh.SetData<string>("ibisstatus", "destination");
				NAPI.ClientEvent.TriggerClientEvent(player, "driverClearBlipAndCol", true);

				NAPI.ClientEvent.TriggerClientEvent(player, "drawHud", "destination", null, null, null, playerVeh.GetData<string>("destination"), false);
			}
		}

		[RemoteEvent("ibisStopArrival")]
		public void ibisStopArrival(Player player)
		{
			setStop(player, "arrival");
		}

		[RemoteEvent("ibisNextStop")]
		public void ibisNextStop(Player player)
		{
			setStop(player, "forward");
		}

		[RemoteEvent("ibisPreviousStop")]
		public void ibisPreviousStop(Player player)
		{
			setStop(player, "backwards");
		}

		public List<vehicleStopList> getVehicleStopList(Vehicle vehicle, bool skipold = false) {
			List<vehicleStopList> vehicleStops = new List<vehicleStopList>();

			int activeRouteDataIndex;
			// Get current Route
			if ((activeRouteDataIndex = Globals.onRoute.FindIndex(obj => obj.vehicleID == vehicle.Value)) != -1)
			{
				active activeRouteData = Globals.onRoute.ElementAt(activeRouteDataIndex);
				// NAPI.Util.ConsoleOutput("Route departure time: " + activeRouteData.beginTime);
				int currentStopID = activeRouteData.currentStopID;

				int busLineIndex = -1;
				int routeIndex = -1;
				int stopIndex = 0;

				// Find current line
				if ((busLineIndex = Globals.busLines.FindIndex(obj => obj.lineID == Globals.onRoute[activeRouteDataIndex].lineID)) != -1)
				{
					// Find current route
					if ((routeIndex = Globals.busLines[busLineIndex].routes.FindIndex(obj => obj.routeID == Globals.onRoute[activeRouteDataIndex].routeID)) != -1)
					{
						// Loop through all stops in the line
						foreach (lineStop stop in Globals.busLines[busLineIndex].routes[routeIndex].stopList)
						{
							stopIndex++;

							//Do not add this stop if it's older than current stop
							if (skipold) {
								if (stopIndex < Globals.busLines[busLineIndex].routes[routeIndex].stopList.FindIndex(obj => obj.stopID == currentStopID))
								{
									continue;
								}
							}
							stop stopData = Globals.busStops.First(item => item.ID == stop.stopRef);

							// NAPI.Util.ConsoleOutput("Adding stop: " + stopData.label + " Departure: " + getBusDeparture(vehicle.Value, stop.stopID).ToUniversalTime().ToString("O") + " Current: " + (currentStopRef == stop.stopRef ? true : false ));
							vehicleStops.Add(new vehicleStopList(
								stopData.label, // stopName
								getBusDeparture(vehicle.Value, stop.stopID).ToUniversalTime().ToString("O"), // stopTime
								(currentStopID == stop.stopID ? true : false ) // currentStop
							));
						}
					}
				}
			}

			return vehicleStops;
		}

		public void setStop(Player player, string method)
		{
			Vehicle playerVeh = NAPI.Player.GetPlayerVehicle(player);

			// This is needed incase the player leaves the bus at the bus stop, the script may want to trigger next stop because client side is not unloaded yet
			if (playerVeh == null) {
				return;
			}

			int activeRouteDataIndex;
			if ((activeRouteDataIndex = Globals.onRoute.FindIndex(obj => obj.vehicleID == playerVeh.Value)) != -1)
			{

				active activeRouteData = Globals.onRoute.ElementAt(activeRouteDataIndex);
				List<Player> InVehicle = NAPI.Pools.GetAllPlayers().FindAll(x => x.Vehicle == playerVeh);
				int currentStopRef = activeRouteData.currentStopRef;
				int currentStopID = activeRouteData.currentStopID;

				int busLineIndex;
				int routeIndex = -1;
				int stopIndex = -1;

				int newStopIndex = 0;
				int newStopReference = 0;
				int newStopID = 0;
				bool newStopExists = false;

				if ((busLineIndex = Globals.busLines.FindIndex(obj => obj.lineID == Globals.onRoute[activeRouteDataIndex].lineID)) != -1)
				{
					if ((routeIndex = Globals.busLines[busLineIndex].routes.FindIndex(obj => obj.routeID == Globals.onRoute[activeRouteDataIndex].routeID)) != -1)
					{
						if ((stopIndex = Globals.busLines[busLineIndex].routes[routeIndex].stopList.FindIndex(obj => obj.stopID == currentStopID)) != -1)
						{
							if (method == "forward")
							{
								newStopIndex = stopIndex + 1;
								if (Globals.busLines[busLineIndex].routes[routeIndex].stopList.Count > newStopIndex)
								{
									newStopReference = Globals.busLines[busLineIndex].routes[routeIndex].stopList[newStopIndex].stopRef;
									newStopID = Globals.busLines[busLineIndex].routes[routeIndex].stopList[newStopIndex].stopID;
									newStopExists = true;
								}
							}
							else if (method == "backwards")
							{
								newStopIndex = stopIndex - 1;
								if (newStopIndex >= 0)
								{
									newStopReference = Globals.busLines[busLineIndex].routes[routeIndex].stopList[newStopIndex].stopRef;
									newStopID = Globals.busLines[busLineIndex].routes[routeIndex].stopList[newStopIndex].stopID;
									newStopExists = true;
								}
							} else if (method == "arrival")
							{
								newStopIndex = stopIndex + 1;
								if (Globals.busLines[busLineIndex].routes[routeIndex].stopList.Count > newStopIndex)
								{
									newStopReference = Globals.busLines[busLineIndex].routes[routeIndex].stopList[newStopIndex].stopRef;
									newStopID = Globals.busLines[busLineIndex].routes[routeIndex].stopList[newStopIndex].stopID;
									newStopExists = true;
								}
							}
						}
					}
				}
				string notificationMessage = "";
				stop currentStop = Globals.busStops.First(item => item.ID == currentStopRef);
				if (newStopExists)
				{
					stop newStop = Globals.busStops.First(item => item.ID == newStopReference);
					if (method != "arrival") {
						Globals.onRoute[activeRouteDataIndex] = new active(playerVeh.Value, busLineIndex, routeIndex, newStopID, newStopReference, Globals.onRoute[activeRouteDataIndex].beginTime, Globals.onRoute[activeRouteDataIndex].operatingMode);

						NAPI.ClientEvent.TriggerClientEvent(player, "drawHud", "operation", newStop.pX, newStop.pY, newStop.pZ, JsonConvert.SerializeObject(getVehicleStopList(playerVeh, true)));
					}

					if (method == "forward")
					{
						notificationMessage = "Next stop ~g~" + newStop.label;
					}
					else if (method == "backwards")
					{
						notificationMessage = "This is ~g~" + newStop.label + "~w~, next stop ~g~" + currentStop.label;
					}
					else if (method == "arrival")
					{
						notificationMessage = "This is ~g~" + currentStop.label + "~w~, next stop ~g~" + newStop.label;
					}
				}
				else if (method == "forward" || method == "arrival")
				{
					this.endLine(playerVeh, activeRouteDataIndex);
					NAPI.ClientEvent.TriggerClientEvent(player, "driverClearBlipAndCol", true);
					NAPI.ClientEvent.TriggerClientEvent(player, "drawHud", "destination", null, null, null, playerVeh.GetData<string>("destination"), false);

					notificationMessage = "This is ~g~" + currentStop.label + "~w~ - FINAL STOP";
				}
				else if (method == "backwards")
				{
					NAPI.Chat.SendChatMessageToPlayer(player, "~r~Cannot go to previous stop, you already have first one selected!");
				}


				foreach (Player passenger in InVehicle)
				{
					if (newStopExists)
					{
						NAPI.Chat.SendChatMessageToPlayer(passenger, "Bus speaker", notificationMessage);
						// NAPI.Notification.SendNotificationToPlayer(passenger, notificationMessage);
					}
					else if (method == "forward" || method == "arrival")
					{
						NAPI.Chat.SendChatMessageToPlayer(passenger, "Bus speaker", notificationMessage);
						// NAPI.Notification.SendNotificationToPlayer(passenger, notificationMessage);
					}
				}
			}
		}

		/* A function created to get a single departures from a bus route, and then return it to the respective client.
		Usually used only when the client requests a spercific departure time, instead of a whole routes departure times.*/
		public DateTime getBusDeparture(int busID, int stopID)
		{
			active currentActive = Globals.onRoute.Find(obj => obj.vehicleID == busID);

			line busLine = Globals.busLines.First(obj => obj.lineID == currentActive.lineID);
			lineRoute route = busLine.routes.First(obj => obj.routeID == currentActive.routeID);
			// NAPI.Util.ConsoleOutput("Line: " + busLine.label + " Route: " + route.version);

			int sumOffset = 0;
			foreach(lineStop lStop in route.stopList) {
				sumOffset += lStop.offset;
				if (lStop.stopID == stopID) {
					break;
				}
			}

			return currentActive.beginTime.AddMinutes(sumOffset);
		}

		/* This function is used to send all the clients in the client-pool a directive, that the given bus vehicle will
		no longer be serving a line. */
		private void endLine(Vehicle vehicle, int routeIndex = -1) {
			if (routeIndex != -1) {
				foreach(Player player in NAPI.Pools.GetAllPlayers())
				{
					NAPI.ClientEvent.TriggerClientEvent(player, "expireBusDestination", vehicle.Value, false);
				}
				//NAPI.ClientEvent.TriggerClientEventForAll("expireBusDestination", vehicle.Value, false);  // Doesn't work after the latest RageMP updates anymore
				Globals.onRoute.RemoveAt(routeIndex);
				//NAPI.Data.ResetEntityData(vehicle, "destination");
				this.setBusTerminus(vehicle, null, "Not in Service");
			} else {
				foreach(Player player in NAPI.Pools.GetAllPlayers())
				{
					NAPI.ClientEvent.TriggerClientEvent(player, "expireBusDestination", vehicle.Value, true);
				}
				//NAPI.ClientEvent.TriggerClientEventForAll("expireBusDestination", vehicle.Value, true);  // Doesn't work after the latest RageMP updates anymore
				vehicle.ResetData("destination");
			}
		}

		/* This function is used to send all the clients in the client-pool a directive, that the given bus vehicle will be serving
		a given line with a given route (terminus). */
		private void setBusTerminus(Vehicle vehicle, string lineNumber, string terminus)
		{
			if (lineNumber != null)
			{
				vehicle.SetData<string>("destination", "[" + lineNumber + "]  " + terminus);
			} else
			{
				vehicle.SetData<string>("destination", terminus);
			}

			// NAPI.Chat.SendChatMessageToAll("~r~Destination: " + vehicle.GetData<string>("destination"));
			foreach(Player player in NAPI.Pools.GetAllPlayers())
			{
				NAPI.ClientEvent.TriggerClientEvent(player, "setBusDestination", vehicle, new Vector3(0, 0, 3.5), vehicle.GetData<string>("destination"));
			}
			//NAPI.ClientEvent.TriggerClientEventForAll("setBusDestination", vehicle, new Vector3(0, 0, 3.5), "[" + lineNumber + "]  " + terminus); // Doesn't work after the latest RageMP updates anymore
		}
	}
}