# Notes and adjustments needed
- The time system of the script is hardcoded to realtime UTC 0, as far as I know that should be the server time. If this needs to be changed, well it will be tricky, but this was the only way to make it reliable.
- There are 2 kinds of stop ID's in the system:
	- `stop.ID` - this is the ID of the stop
	- `lineStop.stopID` - this is the internal ID in linestop, it has no relation to `stop.ID`, `lineStop.stopRef` is the one that links the stop on the line with the `stop` object.
- I couldn't make the app (`transitapp`) window movable, if I did, the map scrolling didn't work anymore
- All existing commands are listed as comment at the beginning of `Core.cs`

## What certainly needs adjusting
### `BusStops.cs`, function `createBusStop`
In case of LC server, `placedObject` needs to problably be diffent object model.

### `Helper.cs`, function `isBus`
Again, in case of LC server, the bus model hashes are problably different.

### `Helper.cs`, function `doesUserHaveManagingAccess`
Premade function, that should check from rest of the GTAW script required permissions.
- Problably admins should have access to these commands
- Bus faction leaders:
	- In case of LS server, the bus faction is part of LSGOV faction, and I'd recon this permissions should be given to ranks 6 and above.
	- Not sure how it's in LC server, I know the bus faction used to be part of the government faction, but I'm not sure the current state

### `Helper.cs`, function `doesUserHaveBusDriverAccess`
I personally think it's the best that that only the correct faction members members (so on LS server, LSGOV faction members) should be able to control the bus systems and drive on the routes. If LFM wants to open up this possibility to others also, then I'd suggest we do it later when we make sure there are no big issues when the script is runnin on the server with many players. I personally have never thought that an RP server should have more than 1 faction/business focusing on bus as it's quite niche job and hard to find drivers. Plus if we do open it up, we might need to look into adding route ownerships.

### Storing of the json files used by the script.
Currently the script is configured to store and read the files from `Globals.resourceFolder + "/json"`, but I imagine for server your might want to have some other path.

This path is used at following places:
- `Core.cs` myResourceStart()
- `BusStops.cs` storeStops() & reloadStops()
- `Lines.cs` loadLines() & storeLines()

# Steps to enable:

config.json file:
```json
"csharp" : "true",
```

client_packages/index.js add:
```
require("buses/index.js")
```

dotnet/settings.xml add:
```xml
  <resource src="bus" />
```

# Development tools
## vscode
### Extensions:
- C# by Microsof
- NuGet Package Manager by jmrog
- vscode-solution-explorer by Fernando Escolar