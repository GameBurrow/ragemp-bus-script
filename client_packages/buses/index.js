let busStopBlip;
let busStopCol;

let driverHud;
let appHud;
let managerHud;

let vehicles = [];
let labelText;
let departureRefreshed = 0;

/* GLOBAL */

mp.keys.bind(0x72, true, function() { //Toggles Mouse With F3
    if (mp.gui.cursor.visible) {
        mp.gui.cursor.visible = false;
    } else {
        mp.gui.cursor.visible = true;
    }
});

mp.events.add('render', () => {
    refreshDests();
});


/* DRIVER HUD */

mp.events.add("drawHud", (systemstatus, stopX, stopY, stopZ, data, waypointSet = true, worldTime = null) => {
    if (!driverHud) {
        driverHud = {
            browser: mp.browsers.new('package://buses/driverhud.html'),
        }
    }

    // driverHud.browser.execute('console.log("' + worldTime + '")')
    if (worldTime) {
        driverHud.browser.execute('initClock("' + worldTime + '")');
    }

    if (systemstatus && systemstatus == "selectline") {
        driverHud.browser.execute('linelist = ' + data)
    }

    if (systemstatus && systemstatus == "destination") {
        driverHud.browser.execute('destination = "' + data + '"')
    }

    if (systemstatus && systemstatus == "selectdest") {
        driverHud.browser.execute('destlist = ' + data)
    }

    if (systemstatus && systemstatus == "operation") {
        driverHud.browser.execute('stoplist = ' + data)
    }
    driverHud.browser.execute('systemmode = "' + systemstatus + '"')
    driverHud.browser.execute('toggleMode()')

    if (waypointSet) {
        if (typeof busStopBlip !== 'undefined') {
            busStopBlip.destroy();
            busStopBlip = undefined;
        }
        if (typeof busStopCol !== 'undefined') {
            busStopCol.destroy();
            busStopCol = undefined;
        }

        busStopBlip = mp.blips.new(513, new mp.Vector3(stopX, stopY, stopZ), {
            name: "Next bus stop",
            color: 57,
        });
        busStopBlip.setRoute(true);

        busStopCol = mp.colshapes.newCircle(stopX, stopY, 20.0);
    }
});

mp.events.add('ibisToggle', (status) => {
    mp.events.callRemote("ibisToggle", status);
})


mp.events.add('ibisOpenLineSelect', (selectedOperatingMode) => {
    mp.events.callRemote("ibisOpenLineSelect", selectedOperatingMode);
})


mp.events.add('ibisSelectLine', (lineID, routeID, startTime, selectedOperatingMode) => {
    mp.events.callRemote("ibisSelectLine", lineID, routeID, startTime, selectedOperatingMode);
})

mp.events.add('ibisOpenDestSelect', () => {
    mp.events.callRemote("ibisOpenDestSelect");
})

mp.events.add('ibisSelectDest', (destination) => {
    mp.events.callRemote("ibisSelectDest", destination);
})

mp.events.add('ibisNextStop', () => {
    mp.events.callRemote("ibisNextStop");
})

mp.events.add('ibisPreviousStop', () => {
    mp.events.callRemote("ibisPreviousStop");
})

mp.events.add('ibisClearLine', () => {
    mp.events.callRemote("ibisClearLine");
})

mp.events.add('playerEnterColshape', (shape) => {
    if (shape.id == busStopCol.id) {
        mp.events.callRemote("ibisStopArrival");
    }

    if (typeof busStopBlip !== 'undefined') {
        busStopBlip.setRoute(false);
    }
});

mp.events.add('playerExitColshape', (shape) => {
    if (shape.id == busStopCol.id) {
        mp.events.callRemote("ibisNextStop");
    }
});

mp.events.add("driverClearBlipAndCol", (keepHud = false) => {
    if (typeof busStopBlip !== 'undefined') {
        busStopBlip.destroy();
        busStopBlip = undefined;
    }
    if (typeof busStopCol !== 'undefined') {
        busStopCol.destroy();
        busStopCol = undefined;
    }
    if (driverHud && keepHud == false) {
        driverHud.browser.destroy();
        driverHud = undefined;
        mp.gui.cursor.visible = false;
    }
});

mp.events.add("ibisPlayBeep", () => {
	mp.game.audio.playSoundFrontend(-1, "5_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET", true);
});

/* BUS DESTINATION DISPLAYS */

mp.events.add("expireBusDestination", (vehicleID, logOff) => {
    arrayIndex = 0;
    while (vehicles.length > arrayIndex) {
        if (vehicles[arrayIndex][0].id == vehicleID) {
            if (logOff) {
                if (vehicles[arrayIndex][4] !== null) {
                    vehicles[arrayIndex][4].destroy();
                    vehicles.splice(arrayIndex, 1);
                }
            }
            /*else {
            	vehicles[arrayIndex][2] = "[GOBus] Not in Service";
            }*/
            break;
        }
        arrayIndex = arrayIndex + 1;
    }
});

mp.events.add("setBusDestination", (vehicle, offset, labeltext) => {
    // mp.gui.chat.push("Destination set to " + labeltext);
    var busOnRoute = false;
    vehicles.forEach(bus => {
        if (bus[0].id == vehicle.id) {
            // mp.gui.chat.push("Bus already in list");
            busOnRoute = true;
            bus[2] = labeltext;
        }
    });
    if (busOnRoute == false) {
        // mp.gui.chat.push("Adding bus to vehicle list");
        vehicles.push([vehicle, offset, labeltext, 1, null]);
    }
});

function refreshDests() {
    //mp.gui.chat.push("Refreshing destinations");
    vehicles.forEach(bus => {
        // mp.gui.chat.push("Updating bus")
        if ((dist = mp.game.system.vdist2(bus[0].position.x, bus[0].position.y, bus[0].position.z, mp.players.local.position.x, mp.players.local.position.y, mp.players.local.position.z)) < 480) {
            let pos = bus[0].getOffsetFromInWorldCoords(bus[1].x, bus[1].y, bus[1].z);
            // mp.gui.chat.push("Setting display")
            // mp.gui.chat.push("Postion X:" + pos.x + " Y " + pos.y + " Z " + pos.z)

            if (bus[4] === null) {
                // mp.gui.chat.push("Creating label")
                bus[4] = mp.labels.new(bus[2], pos, {
                    drawDistance: 50,
                    color: [0, 255, 0, 255],
                    los: false
                })
            } else {
                // mp.gui.chat.push("Recreating label")
                bus[4].text = bus[2]
                bus[4].position = pos
            }
        } else {
            if (bus[4] !== null) {
                // mp.gui.chat.push("Out of range")
                bus[4].destroy();
                bus[4] = null;
            }
        }
    });
}

/* TRANSIT APP */

mp.events.add("openTransitApp", (stoplist, playerPos, linelist, activeBuses, worldTime = null) => {
    if (!appHud) {
        appHud = {
            browser: mp.browsers.new('package://buses/transportapp.html'),
        }
    }

    if (worldTime) {
        appHud.browser.execute('initClock("' + worldTime + '")');
    }

    appHud.browser.execute('playerpos = ' + playerPos)
    appHud.browser.execute('stoplist = ' + stoplist)
    appHud.browser.execute('linelist = ' + linelist)
    appHud.browser.execute('activebuses = ' + activeBuses)

    appHud.browser.execute('refreshHud()')
    mp.gui.cursor.visible = true;
})

mp.events.add("openTransitAppStopView", (stopID, stoplist, playerPos, linelist, activeBuses, worldTime = null) => {
    if (!appHud) {
        appHud = {
            browser: mp.browsers.new('package://buses/transportapp.html'),
        }
    }

    if (worldTime) {
        appHud.browser.execute('initClock("' + worldTime + '")');
    }

    appHud.browser.execute('playerpos = ' + playerPos)
    appHud.browser.execute('stoplist = ' + stoplist)
    appHud.browser.execute('linelist = ' + linelist)
    appHud.browser.execute('activebuses = ' + activeBuses)

    appHud.browser.execute('refreshHud()')
    appHud.browser.execute('setViewToStop(' + stopID + ')')
    mp.gui.cursor.visible = true;
})

mp.events.add("closeTransitApp", () => {
    if (appHud) {
        appHud.browser.destroy();
        appHud = undefined;
    }
    mp.gui.cursor.visible = false;
});

mp.events.add("getLiveBuses", () => {
    if (appHud) {
        mp.events.callRemote("getLiveBuses");
    }
});

mp.events.add("updateLiveBuses", (activeBuses) => {
    if (appHud) {
        appHud.browser.execute('activebuses = ' + activeBuses)
        appHud.browser.execute('updateActiveBusesOnMap()')
    }
});


/* TRANSIT MANAGER */

mp.events.add("openTransitManager", (stoplist, linelist, view = "lineview", responseType = null, responseMessage = null, extraData = null, messageUpdateOnly = "no") => {
    if (!managerHud) {
        managerHud = {
            browser: mp.browsers.new('package://buses/transportmanager.html'),
        }
    }

	if (extraData) {
		extraData = JSON.parse(extraData);
	}

	if (responseType != null) {
		managerHud.browser.execute('responsetype = "' + responseType + '"')
		managerHud.browser.execute('responsemessage = "' + responseMessage + '"')
	} else {
		managerHud.browser.execute('responsetype = null')
		managerHud.browser.execute('responsemessage = null')
	}

    managerHud.browser.execute('stoplist = ' + stoplist)
    managerHud.browser.execute('linelist = ' + linelist)
	if (view == "routeedit") {
		managerHud.browser.execute('editLine = ' + extraData.lineID)
		managerHud.browser.execute('editRoute = ' + extraData.routeID)
	}

	if (messageUpdateOnly == "yes") {
		managerHud.browser.execute('showMessage()')
	} else {
		managerHud.browser.execute('setView("' + view + '")')
	}

    mp.gui.cursor.visible = true;
})

mp.events.add("closeTransitManager", () => {
    if (managerHud) {
        managerHud.browser.destroy();
        managerHud = undefined;
    }
    mp.gui.cursor.visible = false;
});

mp.events.add("addline", (lineLabel) => {
    mp.events.callRemote("transitManagerAddLine", lineLabel);
});

mp.events.add("editline", (lineID, lineLabel) => {
    mp.events.callRemote("transitManagerEditLine", lineID, lineLabel);
});

mp.events.add("deleteline", (lineID) => {
    mp.events.callRemote("transitManagerDeleteLine", lineID);
});

mp.events.add("addroute", (lineID, lineLabel) => {
    mp.events.callRemote("transitManagerAddRoute", lineID, lineLabel);
});

mp.events.add("editroute", (lineID, routeID, routeLabel, stoplist) => {
    mp.events.callRemote("transitManagerEditRoute", lineID, routeID, routeLabel, stoplist);
});

mp.events.add("toggleroutestatus", (lineID, routeID,) => {
    mp.events.callRemote("transitManagerToggleRoute", lineID, routeID);
});

mp.events.add("deleteRoute", (lineID, routeID) => {
    mp.events.callRemote("transitManagerDeleteRoute", lineID, routeID);
});

mp.events.add("deleteRouteStop", (lineID, routeID, stopID) => {
    mp.events.callRemote("transitManagerDeleteRouteStop", lineID, routeID, stopID);
});

mp.events.add("addroutestopnearest", (lineID, routeID) => {
    mp.events.callRemote("transitManagerAddRouteStopNearest", lineID, routeID);
});
