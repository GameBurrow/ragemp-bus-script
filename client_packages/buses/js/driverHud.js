var systemmode = "off";
var delay = "off";
var selectedOperatingMode = "normal";

var clock;
var clockHourOffset = 0;

var beepPlayed = 0;
var resetBeepCount = false;

var linelist = null;
var destlist = null;
var destination = null;
var stoplist = null

var selectedLineID = null;
var selectedRouteID = null;


$(function() {
    toggleMode()

    $('.powerbutton').click(() => {
        if (systemmode == "off") {
            mp.trigger("ibisToggle", "on");
        } else {
            mp.trigger("ibisToggle", "off");
        }
    });

    $('#setline').click(() => {
		systemmode="selectoperatingmode"
        toggleMode()
    });

    $("body").on('click', '.lineselectbox > ul > .btn-lineselect', function() {
        selectedLineID = $(this).data('lineid');
        selectedRouteID = $(this).data('routeid');

        drawStartimeList();
        //mp.trigger("ibisSelectLine", $(this).data('lineid'), $(this).data('routeid'))
    })
    $("body").on('click', '.lineselectbox > ul > .btn-operatingmodeselect', function() {
        selectedOperatingMode = $(this).data('mode');
		mp.trigger("ibisOpenLineSelect", selectedOperatingMode);
    })

    $("body").on('click', '.lineselectbox > ul > .btn-timeselect', function() {
        mp.trigger("ibisSelectLine", selectedLineID, selectedRouteID, $(this).data('starttime'), selectedOperatingMode)
    })



    $('#setdest').click(() => {
        mp.trigger("ibisOpenDestSelect");
    });

    $("body").on('click', '.lineselectbox > ul > .btn-destselect', function() {
        mp.trigger("ibisSelectDest", $(this).data('destination'))
    })

    $('#nexstop').click(() => {
        mp.trigger("ibisNextStop");
    });

    $('#prevstop').click(() => {
        mp.trigger("ibisPreviousStop");
    });

    $('#clearroute').click(() => {
        mp.trigger("ibisClearLine");
    });

    // Make the DIV element draggable:
    dragElement(document.getElementById("hudbox"));
})

function toggleMode() {
    $('.contentdisplay > .turnedoff').hide();
    $('.contentdisplay > .destination').hide();
    $('.contentdisplay > .stopscreen').hide();
    $('.contentdisplay > .lineselect').hide();
    $('.contentdisplay > .turnedoff').hide();

    if (systemmode == "off") {
        $('.bottomrow').hide();

        $('.contentdisplay > .turnedoff').show();
    } else if (systemmode == "destination") {
        $('.bottomrow').show();

        $('.contentdisplay > .destination > h3').text(destination);
        $('.contentdisplay > .destination').show();
    } else if (systemmode == "operation") {
		//Reset beep sound to notifiy player of departure since UI update was triggered which means either stop changes
		// or driver entered the vehicle
		resetBeepCount = true;
        $('.bottomrow').show();

        drawStopList();

        $('.contentdisplay > .stopscreen').show();
    } else if (systemmode == "selectoperatingmode") {
		$('.lineselect > h4').text("Select operating mode")

		$('.lineselectbox > ul').empty()
		$('.lineselectbox > ul').append('<li class="btn btn-operatingmodeselect" data-mode="normal">Normal mode</li>')
		$('.lineselectbox > ul').append('<li class="btn btn-operatingmodeselect" data-mode="training">Training mode</li>')
		$('.lineselectbox > ul').append('<li class="btn btn-operatingmodeselect" data-mode="testing">Test mode</li>')

		$('.contentdisplay > .lineselect').show();
    } else if (systemmode == "selectline") {
        $('.bottomrow').show();

        drawLineList();
        $('.lineselect > h4').text("Select line")

        $('.contentdisplay > .lineselect').show();
    } else if (systemmode == "selectdest") {
        $('.bottomrow').show();

        drawDestList();
        $('.lineselect > h4').text("Select destination")

        $('.contentdisplay > .lineselect').show();
    }
    refreshDelay()
}

function drawLineList() {
    $('.lineselectbox > ul').empty()

    $.each(linelist, (lineindex, line) => {
        $('.lineselectbox > ul')
            .append('<li class="btn btn-lineselect" data-lineid=' + line.lineID + ' data-routeid=' + line.routeID + '>' + line.lineLabel + ' to ' + line.destination + '</li>');
    })
}

function drawStartimeList() {
    $('.lineselect > h4').text("Select start time")
    $('.lineselectbox > ul').empty()

    var startTimeOptions = []

    var nearestTime = new Date(clock);
    nearestTime.setSeconds(0)
    let currentMinutes = nearestTime.getMinutes();

    minutesToAdd = 5 - (currentMinutes % 5);
    nearestTime.setMinutes(currentMinutes + minutesToAdd);


    var tempTime = new Date(nearestTime);
    tempTime.setHours(tempTime.getHours() - 1);
    startTimeOptions.push(new Date(tempTime));

    // Generate times for 3 hours (each our has 12 options)
    for (let i = -11; i < (2 * 12); i++) {
        tempTime.setMinutes(tempTime.getMinutes() + 5)
        startTimeOptions.push(new Date(tempTime));
    }

    $.each(startTimeOptions, (index, startTime) => {
        $('.lineselectbox > ul')
            .append('<li class="btn btn-timeselect ' + (startTime.getHours() == nearestTime.getHours() && startTime.getMinutes() == nearestTime.getMinutes() ? 'neareststarttime' : '') + '" data-starttime=' + startTime.toISOString() + '>' + String(startTime.getHours()).padStart(2, '0') + ':' + String(startTime.getMinutes()).padStart(2, '0') + '</li>');
    })

    $(".neareststarttime")[0].scrollIntoView();

    tempTime = null;
    nearestTime = null;
    startTimeOptions = [];
}

function drawDestList() {
    $('.lineselectbox > ul').empty()

    //console.log(destlist)

    $.each(destlist, (destindex, destination) => {
        $('.lineselectbox > ul')
            .append('<li class="btn btn-destselect" data-destination="' + destination + '">' + destination + '</li>');
    })
}

function drawStopList() {
    $('.stopscreen > ul').empty()

    $.each(stoplist, (stopindex, stop) => {
        stopTime = new Date(stop.stopTime);
        // console.log(stop.stopName + ": " + stopTime);

        $('.stopscreen > ul')
            .append('<li class="stop ' + (stop.currentStop ? 'current' : null) + '"><span class="departuretime">' + String(stopTime.getHours()).padStart(2, '0') + ':' + String(stopTime.getMinutes()).padStart(2, '0') + '</span> ' + stop.stopName + '</li>');
    })

}

function initClock(serverTimeString = null) {
    if (serverTimeString) {
        clock = new Date(serverTimeString);
    } else {
        clock = new Date();
    }

    clientTime = new Date()

    clockHourOffset = clock.getHours() - clientTime.getHours();

    $('.toprow > .time').text(String(clock.getHours()).padStart(2, '0') + ':' + String(clock.getMinutes()).padStart(2, '0'));
    refreshClock();
}

function refreshClock() {
    setInterval(() => {
        clock = new Date();
        clock.setHours(clock.getHours() + clockHourOffset);

        $('.toprow > .time').text(String(clock.getHours()).padStart(2, '0') + ':' + String(clock.getMinutes()).padStart(2, '0'));
        refreshDelay()
    }, 5000)
}

function refreshDelay() {
    if (stoplist) {
        $.each(stoplist, (stopindex, stop) => {
            if (stop.currentStop) {
                stopTime = new Date(stop.stopTime);
                diff = (stopTime.getTime() - clock.getTime()) / 1000;
                diff /= 60;

				// If the driver is already late, let's not make the system beep again
				if (resetBeepCount && diff > 0) {
					beepPlayed = 0;
					resetBeepCount = false;
				}

				if (diff < 0 && beepPlayed < 5){
					mp.trigger("ibisPlayBeep");
					beepPlayed++;
				}

                $('.toprow > .delay > .delayminutes').text(Math.round(diff));
                $('.toprow > .delay').show();
                return;
            }
        })
    }
}

function dragElement(elmnt) {
    var pos1 = 0,
        pos2 = 0,
        pos3 = 0,
        pos4 = 0;
    // otherwise, move the DIV from anywhere inside the DIV:
    elmnt.onmousedown = dragMouseDown;

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // set the element's new position:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    }

    function closeDragElement() {
        // stop moving when mouse button is released:
        document.onmouseup = null;
        document.onmousemove = null;
    }
}