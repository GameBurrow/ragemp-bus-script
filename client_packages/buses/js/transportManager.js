var responsetype;
var responsemessage;

var stoplist;
var linelist;

var editLine;
var editRoute;
var delRouteStop;

var delLine;
var delRoute;

$(function() {
    $('#closeBtn').click(() => {
        mp.trigger("closeTransitManager");
    });

    $('#homeBtn').click(() => {
        setView("lineview");
    });

    $('.js-confirmyes').click(() => {
        if (delRouteStop != null && editLine != null && editRoute != null) {
			mp.trigger("deleteRouteStop", editLine, editRoute, delRouteStop);
			delRouteStop = null;
		} else if (delLine != null && delRoute == null) {
			mp.trigger("deleteline", delLine);
			delLine = null;
		} else if (delLine != null && delRoute != null) {
			mp.trigger("deleteRoute", delLine, delRoute);
			delLine = null;
			delRoute = null;
		}
    });

    $('.js-confirmno').click(() => {
        setView("lineview");
    });

    $("body").on('click', '.js-input', function() {
        $(this).focus(); // No idea why this is needed
    })


    $("body").on('click', '.js-addline', function() {
        editLine = null;
		editRoute = null;

        setView("lineadd");
    })

    $("body").on('click', '.js-linedeletebtn', function() {
        delLine = $(this).parents('li').first().attr('data-lineID');
		delRoute = null;

        setView("linedel");
    })

    $("body").on('click', '.js-lineeditbtn', function() {
        editLine = $(this).parents('li').first().attr('data-lineID');
		editRoute = null;
        setView("linedit")
    })

    $("body").on('click', '.js-saveline', function() {
		linelabel = $('#lineEdit').find('#lineLabel').val();

        if (linelabel == null || typeof linelabel === undefined || linelabel.length < 1) {
			$('.error').html('<i class="fas fa-exclamation-circle"></i> Line label needs be at least 1 symbol long').show();
        } else {
			if (editLine == null) {
				mp.trigger("addline", linelabel);
            } else if (editLine != null) {
				mp.trigger("editline", editLine, linelabel);
			}

        }
    })

	$("body").on('click', '.js-saveroute', function() {
		showMessage();
		routelabel = $('#routeEdit').find('#routeLabel').val();

        if (routelabel == null || typeof routelabel === undefined || routelabel.length < 1) {
			$('.error').html('<i class="fas fa-exclamation-circle"></i> Label needs be at least 1 symbol long').show();
        } else {
			if (editLine != null && editRoute == null) {
				mp.trigger("addroute", editLine, routelabel);
			} else if (editLine != null && editRoute != null) {
				var newStopList = [];

				$('#routeEdit > ul > li').each((index, stopItem) => {
					newStopList.push({
						stopRef: $(stopItem).attr('data-stopRef'),
						stopID: $(stopItem).attr('data-stopID'),
						offset: $(stopItem).find(".stopOffset").first().val(),
					})
				})

				mp.trigger("editroute", editLine, editRoute, routelabel, JSON.stringify(newStopList));
			}
		}


	})

    $("body").on('click', '.js-routeaddbtn', function() {
        editLine = $(this).parents('li').first().attr('data-lineID');
		editRoute = null;

        setView("routeadd")
    })

    $("body").on('click', '.js-routeeditbtn', function() {
        editLine = $(this).parents('li').first().attr('data-lineID');
        editRoute = $(this).parents('li').first().attr('data-routeID');

        setView("routeedit");
    })

	$("body").on('click', '.js-routetoggle', function() {
        editLine = $(this).parents('li').first().attr('data-lineID');
        editRoute = $(this).parents('li').first().attr('data-routeID');

        mp.trigger('toggleroutestatus', editLine, editRoute);
    })

    $("body").on('click', '.js-routedeletebtn', function() {
		delLine = $(this).parents('li').first().attr('data-lineID');
        delRoute = $(this).parents('li').first().attr('data-routeID');

        setView("routedel");
    })
	$("body").on('click', '.js-routestopremovebtn', function() {
		delRouteStop = $(this).parents('li').first().attr('data-stopID');
		setView("routestopdel");
	})

	$("body").on('click', '.js-addstop', function() {
		if (editLine != null && editRoute != null) {
			mp.trigger("addroutestopnearest", editLine, editRoute);
		}
	})

	$("body").on('change', '.js-stopoffset, #routeLabel', () => {
		$('.warning').html('<i class="fas fa-exclamation-circle"></i> You have unsaved changes!').show();
	})

    // Make the DIV element draggable:
    dragElement(document.getElementById("content"));
})

function setView(view) {
    $('#linelist').hide();
    $('#lineEdit').hide();
    $('#routeEdit').hide();
    $("#confirmWindow").hide()

	showMessage()

    switch (view) {
        case "lineview":
            $("#contentTitle>h3").text("Manage lines")
            openLineListView()
            break;
        case "lineadd":
            $("#contentTitle>h3").text("Add line")
            openLineEditView()
            break;
        case "linedit":
            openLineEditView()
            break;
        case "linedel":
            openDeleteConfirm()
            break;
        case "routeadd":
            $("#contentTitle>h3").text("Adding route")
            openRouteAddView()
            break;
        case "routeedit":
            $("#contentTitle>h3").text("Editing route")
            openRouteEditView()
            break;
        case "routedel":
            openDeleteConfirm()
            break;
		case "routestopdel":
			openDeleteConfirm();
			break;
    }
}

function showMessage() {
	if (responsetype == "error") {
		$('.error').html(responsemessage).show()
	} else {
		$('.error').hide();
	}

	if (responsetype == "warning") {
		$('.warning').html(responsemessage).show()
	} else {
		$('.warning').hide();
	}

	if (responsetype == "info") {
		$('.info').html(responsemessage).show()
	} else {
		$('.info').hide();
	}

	if (responsetype == "success") {
		$('.success').html(responsemessage).show()
	} else {
		$('.success').hide();
	}
	// Reset responsetype to make sure it's not loaded after next restart
	responsetype = null;
}

function openLineListView() {
    $('#linelist > ul').empty();

    $.each(linelist, (lineindex, line) => {
        $('#linelist > ul').append(
            "<li data-lineID=" + line.lineID + ">" +
            "<div class='lineTitle'>Line <span class='js-linelabel'>" + line.label + "</span></div>" +
            " <i class='fas fa-plus js-routeaddbtn'></i>" +
            " <i class='fas fa-edit js-lineeditbtn'></i>" +
            " <i class='fas fa-trash js-linedeletebtn'></i>" +
            "<ul></ul>" +
            "</li>"
        );
        $.each(line.routes, (routeindex, route) => {
            $('#linelist > ul > li[data-lineID=' + line.lineID + '] > ul').append(
                "<li data-lineID=" + line.lineID + " data-routeID=" + route.routeID + " >" +
                "Route " + route.version +
                " <i class='fas " + (route.published ? "fa-toggle-on" : "fa-toggle-off") +  " js-routetoggle'></i>" +
                " <i class='fas fa-edit js-routeeditbtn'></i>" +
                " <i class='fas fa-trash js-routedeletebtn'></i>" +
                "</li>"
            );
        });
    });
    $('#linelist').show();
}

function openRouteAddView() {
    $('#routeEdit > ul').empty();
    if (editLine != null) {
        highestRouteID = 0;

        $.each(linelist, (lineindex, line) => {
            $.each(line.routes, (routeindex, route) => {

                if (route.routeID > highestRouteID) {
                    highestRouteID = route.routeID;
                }
            });
            $('#routeEdit > #routeLabel').val(null);
            $('#routeEdit > .js-addstop').hide();

            $('#routeEdit').show();
        });
    }
}

function openRouteEditView() {
    $('#routeEdit > ul').empty();

    if (editLine != null) {
        $.each(linelist, (lineindex, line) => {
            if (line.lineID == editLine) {

                if (editRoute != null) {
                    $.each(line.routes, (routeindex, route) => {
                        if (route.routeID == editRoute) {
                            $('#routeEdit > #routeLabel').val(route.version);

                            var stopCounter = 0;

                            $.each(route.stopList, (stopindex, stop) => {
                                let stopData = getStopData(stop.stopRef)[0];
                                $('#routeEdit > ul').append(
                                    "<li data-stopID='"+ stop.stopID +"' data-stopRef='"+ stop.stopRef +"'>" +
                                    "<div class='stopName'>" + stopData.label + "</div>" +
                                    (stopCounter != 0 ? " Delay: <input type='number' class='stopOffset js-input js-stopoffset' value='" + stop.offset + "' min='0' />min" : "") +
                                    " <i class='fas fa-trash js-routestopremovebtn'></i>" +
                                    "</li>"
                                );
                                stopCounter++;
                            });
                        }
                    });
                }
            }
        });

        $('#routeEdit > .js-addstop').show();
        $('#routeEdit').show();
    }

}

function openLineEditView() {
    if (editLine != null) {
        $.each(linelist, (lineindex, line) => {
            if (line.lineID == editLine) {
                $("#lineLabel").val(line.label);
                $("#contentTitle>h3").text("Editing line " + line.label);
            }
        })

    } else {
        $("#lineLabel").val(null);
    }

    $('#lineEdit').show();
}

function openDeleteConfirm() {
    if (delLine != null || delRouteStop != null) {
        var text;
        if (delRouteStop != null) {
			text = "Are you sure you want to delete stop ID " + delRouteStop + " from line ID " + editLine + ", route ID " + editRoute + "?"
		} else if (delRoute != null) {
            text = "Are you sure you want to delete line ID " + delLine + " route ID " + delRoute + "?"
        } else {
            text = "Are you sure you want to delete line ID " + delLine + "?"
        }
        $("#confirmWindow > .confirmationtext").html(text);

        $("#contentTitle>h3").text("Delete confirmation");
        $("#confirmWindow").show();
    }
}

function getStopData(stopID) {
    return stoplist.filter(stop => {
        return stop.ID === stopID
    })
}

function dragElement(elmnt) {
    var pos1 = 0,
        pos2 = 0,
        pos3 = 0,
        pos4 = 0;
    // otherwise, move the DIV from anywhere inside the DIV:
    elmnt.onmousedown = dragMouseDown;

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // set the element's new position:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    }

    function closeDragElement() {
        // stop moving when mouse button is released:
        document.onmouseup = null;
        document.onmousemove = null;
    }
}