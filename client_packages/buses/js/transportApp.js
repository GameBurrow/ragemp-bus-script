// Global vars

var clock;
var clockHourOffset = 0;

var playerpos = null;
var stoplist = null;
var linelist = null;
var activebuses = null;

var stopMarkers = {};
var busMarkers = {};
var linePathCoords = []
var linePath = null;

var selectedLineID;
var selectedRouteID;

//Map image settings
const ZeroX = 3757; // ZeroCordinate X pixelpoint on map
const ZeroY = 5522; // ZeroCordinate y pixelpoint on map
const Scale = 1.518611954439182;
const ImageX = 8192;
const ImageY = 8192;

var map = L.map('map', {
    crs: L.CRS.Simple,
    dragging: true,
    minZoom: -3,
    maxZoom: 1,
    attributionControl: false
});


var bounds = [
    [0, 0],
    [ImageY, ImageX]
];


// Image originates here, according to author, they are rockstar's images: https://gtaforums.com/topic/595113-high-resolution-maps-satellite-roadmap-atlas/
var image = L.imageOverlay('img/GTAV_ATLUS_8192x8192.png', bounds).addTo(map);

map.fitBounds(bounds);

// Map icons
const stopIcon = L.divIcon({
    html: '<i class="fa fa-circle"></i>',
    iconSize: [20, 20],
    className: 'mapIconCircle'
})

const busIcon = L.divIcon({
    html: '<i class="fa fa-bus"></i>',
    iconSize: [20, 20],
    className: 'mapIconBus'
})

function drawMarker(X, Y, icon, title) {
    coords = getPictureCoords(X, Y)

    return L.marker([coords.y, coords.x], { icon: icon }).addTo(map).bindTooltip(title);
}

function getPictureCoords(x, y) {
    x = x / Scale
    y = y / Scale
    return {
        x: ZeroX + x,
        y: ImageY - ZeroY + y
    } // Invert Y-axis
}

$(function() {
    $('#closeBtn').click(() => {
        mp.trigger("closeTransitApp");
    });

    $('#homeBtn').click(() => {
        refreshHud();
    });

    $("body").on('click', '.btn-lineselect', function() {
        setViewToRoute($(this).data('line'), $(this).data('route'));
    })

    $("body").on('click', '.btn-viewstop', function() {
        setViewToStop($(this).data('stopid'), );
    })

    // Refresh bus locations every 5 second
    setInterval(() => {
        mp.trigger("getLiveBuses");
    }, 5000);
})


function initClock(serverTimeString = null) {
    if (serverTimeString) {
        clock = new Date(serverTimeString);
    } else {
        clock = new Date();
    }

    clientTime = new Date()

    clockHourOffset = clock.getHours() - clientTime.getHours();

    $('#currentTime').text(String(clock.getHours()).padStart(2, '0') + ':' + String(clock.getMinutes()).padStart(2, '0'));
    refreshClock();
}

function refreshClock() {
    setInterval(() => {
        clock = new Date();
        clock.setHours(clock.getHours() + clockHourOffset);

        $('#currentTime').text(String(clock.getHours()).padStart(2, '0') + ':' + String(clock.getMinutes()).padStart(2, '0'));
    }, 5000)
}

function refreshHud() {
    currentPos = getPictureCoords(playerpos.x, playerpos.y)

    map.setView([currentPos.y, currentPos.x], 0)

    setViewToLineList()
}

function toggleView(view) {
    $("#linelist").hide();
    $("#stoplist").hide();
    $("#stopview").hide();
    if (linePath != null) {
        linePath.remove(map);
        linePath = null;
    }

    switch (view) {
        case "linelist":
            $("#linelist>ul").empty();
            $("#contentTitle>h3").text("Lines")
            break;
        case "lineview":
            $("#stoplist>ul").empty();
            break;
        case "stopview":
            $("#stopview>ul").empty();
            break;
    }
}

function setViewToLineList() {
    selectedLineID = null;
    selectedRouteID = null;

    toggleView("linelist");

    clearMarkers()
    drawStopsOnMap()
    updateActiveBusesOnMap()

    $.each(linelist, (lineindex, line) => {
        $.each(line.routes, (routeindex, route) => {
            if (route.stopList && route.stopList.length > 0 && route.published) {
                let firststop = route.stopList[0];
                let laststop = route.stopList[route.stopList.length - 1];

                $('#linelist > ul').append(
                    "<li class='btn-lineselect' data-line=" + lineindex + " data-route=" + routeindex + ">" +
                    "<div class='line'>" + line.label + " " + getStopData(firststop.stopRef)[0].label + " to " + getStopData(laststop.stopRef)[0].label + "</div> " +
                    "<i class='fas fa-bus'></i> <div class='activeBusCount'>" + countActiveBusesOnRoute(line.lineID, route.routeID) + "</div>" +
                    "</li>"
                )
            }
        })
    })

    $("#linelist").show();
}

function setViewToRoute(line, route) {
    linePathCoords = []
    selectedLineID = linelist[line].lineID;
    selectedRouteID = linelist[line].routes[route].routeID;

    toggleView("lineview");
    fadeNonLineMarkers(linelist[line].lineID, linelist[line].routes[route].routeID);

    var activeBusesOnRoute = getActiveBusesOnRoute(linelist[line].lineID, linelist[line].routes[route].routeID);

    var selectedstoplist = linelist[line].routes[route].stopList;
    let firststop = selectedstoplist[0]
    let laststop = selectedstoplist[selectedstoplist.length - 1];
    $("#contentTitle>h3").text(linelist[line].label + " " + getStopData(firststop.stopRef)[0].label + " to " + getStopData(laststop.stopRef)[0].label);

    if (activeBusesOnRoute.length == 0) {
        $('#stoplist > .warning').show()
    } else {
        $('#stoplist > .warning').hide()
    }

    var currentStopOfBusPassed = {};

    $.each(activeBusesOnRoute, (busindex, bus) => {
        currentStopOfBusPassed[bus.vehicleID] = false
    });

    $.each(selectedstoplist, (stopindex, stop) => {
        stopData = getStopData(stop.stopRef);
        stopDepartures = [];

        var active = false;
        $.each(activeBusesOnRoute, (busindex, bus) => {
            if (bus.activeData.currentStopID == stop.stopID) {
                active = true;
                currentStopOfBusPassed[bus.vehicleID] = true
            }
            if (currentStopOfBusPassed[bus.vehicleID] == true) {
				departureTimes = calculateBusDepartureTimesInStop(selectedstoplist, stop.stopRef, bus.activeData.beginTime)
				$.each(departureTimes, (departureTimeIndex, departureTime) => {
					if (departureTime.stopID == stop.stopID) {
						stopDepartures.push(departureTime.time)
					}
				});
            }
        })

        stopDepartures.sort()

        $('#stoplist > ul').append(
            "<li class='btn-viewstop" + (active ? " active'" : "") + "' data-stopID=" + stop.stopRef + ">" +
            (active ? "<i class='fas fa-bus busOnRoute'></i>" : "") +
            "<div class='stopName'>" + stopData[0].label + "</div> " +
            (stopDepartures.length != 0 ? "<div class='stopTime'>" + stopDepartures.join("</div> <div class='stopTime'>") + "</div>" : "") +
            "</li>"
        )

        stopMapCoords = getPictureCoords(stopData[0].pX, stopData[0].pY);
        linePathCoords.push([stopMapCoords.y, stopMapCoords.x]);
    })

    linePath = L.polyline(linePathCoords).addTo(map);
    $("#stoplist").show();
}

function setViewToStop(stopID) {
    selectedLineID = null;
    selectedRouteID = null;

    toggleView("stopview");
    clearMarkers()
    drawStopsOnMap()
    updateActiveBusesOnMap()

    stopMarkers[stopID].openTooltip()


    stopData = getStopData(stopID)[0];

    $("#contentTitle>h3").text(stopData.label);

    $.each(stopData.relatedStops, (stopindex, stop) => {
        $.each(linelist, (lineindex, line) => {
            if (line.lineID == stop.lineID) {
                $.each(line.routes, (routeindex, route) => {
                    if (route.routeID == stop.routeID) {
                        if (route.stopList && route.stopList.length > 0 && route.published) {
                            let firststop = route.stopList[0];
                            let laststop = route.stopList[route.stopList.length - 1];

                            var activeBusesOnRoute = getActiveBusesOnRoute(stop.lineID, stop.routeID);
                            stopDepartures = [];

                            $.each(activeBusesOnRoute, (busindex, bus) => {
								departureTimes = calculateBusDepartureTimesInStop(route.stopList, stopID, bus.activeData.beginTime)
								$.each(departureTimes, (departureTimeIndex, departureTime) => {
									if (stop.stopID == departureTime.stopID) {
										stopDepartures.push(departureTime.time)
									}
								});
                            });

                            $('#stopview > ul').append(
                                "<li class='btn-lineselect' data-line=" + lineindex + " data-route=" + routeindex + ">" +
                                "<div class='line'>" + line.label + " " + getStopData(firststop.stopRef)[0].label + " to " + getStopData(laststop.stopRef)[0].label + "</div> " +
                                "<i class='fas fa-bus'></i> <div class='activeBusCount'>" + countActiveBusesOnRoute(line.lineID, route.routeID) + "</div><br />" +
                                (stopDepartures.length != 0 ? "<div class='stopTime'>" + stopDepartures.join("</div> <div class='stopTime'>") + "</div>" : "") +
                                "</li>"
                            )
                        }
                    }
                })
            }
        })
    })

    $("#stopview").show();
}

function calculateBusDepartureTimesInStop(stops, stopID, startTime) {
    var delayInMinutes = 0;

	var returnOptions = [];

    $.each(stops, (stopindex, stop) => {
        delayInMinutes = delayInMinutes + stop.offset;
        if (stop.stopRef == stopID) {
			time = new Date(startTime);
    		time.setMinutes(time.getMinutes() + delayInMinutes);

            returnOptions.push({
				'stopID': stop.stopID,
				'time': String(time.getHours()).padStart(2, '0') + ':' + String(time.getMinutes()).padStart(2, '0'),
				'stopNumber': stopindex + 1
			})
        }
    })

	return returnOptions;

    /* time = new Date(startTime);
    time.setMinutes(time.getMinutes() + delayInMinutes);
    return String(time.getHours()).padStart(2, '0') + ':' + String(time.getMinutes()).padStart(2, '0') */
}

function getActiveBusesOnRoute(lineID, routeID) {
    var returnList = []

    $.each(activebuses, (index, bus) => {
        if (bus.activeData.lineID == lineID && bus.activeData.routeID == routeID) {
            returnList.push(bus);
        }
    })

    return returnList
}

function countActiveBusesOnRoute(lineID, routeID) {
    var count = 0;

    $.each(activebuses, (index, bus) => {
        if (bus.activeData.lineID == lineID && bus.activeData.routeID == routeID) {
            count++;
        }
    })

    return count;
}

function getStopData(stopID) {
    return stoplist.filter(stop => {
        return stop.ID === stopID
    })
}

function drawStopsOnMap() {
    $.each(stoplist, (index, stop) => {
        stopMarkers[stop.ID] = drawMarker(stop.pX, stop.pY, stopIcon, "<strong>" + stop.label + "</strong>")
        stopMarkers[stop.ID].on('click', onStopMarkerClick);
    })

    $.each(linelist, (lineindex, line) => {
        $.each(line.routes, (routeindex, route) => {
			if (route.published) {
				let firststop = route.stopList[0];
				let laststop = route.stopList[route.stopList.length - 1];

				$.each(route.stopList, (stopIndex, stop) => {
					existingToolTip = stopMarkers[stop.stopRef].getTooltip().getContent()
					stopMarkers[stop.stopRef].bindTooltip(existingToolTip + "<br />" + line.label + " " + getStopData(firststop.stopRef)[0].label + " to " + getStopData(laststop.stopRef)[0].label)
				})
			}
        })
    })
}

function clearMarkers(type = null) {
    if ((type && type == "stops") || type == null) {
        for (const key in stopMarkers) {
            stopMarkers[key].remove(map);
            delete stopMarkers[key];
        }
    }
    if ((type && type == "buses") || type == null) {
        for (const key in busMarkers) {
            busMarkers[key].remove(map);
            delete busMarkers[key];
        }
    }
}

function fadeNonLineMarkers(lineID, routeID) {
    for (const key in stopMarkers) {
        if (!isStopInList(lineID, routeID, key)) {
            stopMarkers[key].setOpacity(0.2);
        }
        stopMarkers[key].closeTooltip()
    }

    $.each(activebuses, (index, bus) => {
        if (bus.activeData.lineID != lineID || bus.activeData.routeID != routeID) {
            busMarkers[bus.vehicleID].setOpacity(0.2);
        }
    })
}

function isStopInList(lineID, routeID, stopID) {
    var returnType = false;
    $.each(linelist, (index, line) => {
        if (line.lineID == lineID) {
            $.each(line.routes, (routeindex, route) => {
                if (route.routeID == routeID) {
                    tmpList = route.stopList.filter(stop => {
                        return stop.stopRef == stopID
                    })

                    if (tmpList.length > 0) {
                        returnType = true;
                    }
                }
            })
        }
    });

    return returnType;
}


function updateActiveBusesOnMap() {
    $.each(busMarkers, (index, marker) => {
        marker.remove();
    })

    $.each(activebuses, (index, bus) => {
        busMarkers[bus.vehicleID] = drawMarker(bus.vehiclePosX, bus.vehiclePosY, busIcon, getBusMarkerTitle(bus.activeData))

        if (selectedLineID !== null && selectedRouteID !== null) {
            if (bus.activeData.lineID != selectedLineID || bus.activeData.routeID != selectedRouteID) {
                busMarkers[bus.vehicleID].setOpacity(0.2);
            }
        }
    })
}

function getBusMarkerTitle(bus) {
    var markerTitle = ""
    $.each(linelist, (index, line) => {
        if (line.lineID == bus.lineID) {
            $.each(line.routes, (routeindex, route) => {
                if (route.routeID == bus.routeID) {
                    let firststop = route.stopList[0];
                    let laststop = route.stopList[route.stopList.length - 1];

                    markerTitle = "<strong>" + line.label + " " + getStopData(firststop.stopRef)[0].label + " to " + getStopData(laststop.stopRef)[0].label + "</strong><br /> Next stop: " + getStopData(bus.currentStopRef)[0].label
                }
            })
        }
    })

    return markerTitle
}

function onStopMarkerClick(e) {
    for (const key in stopMarkers) {
        if (stopMarkers[key] == e.target) {
            setViewToStop(parseInt(key));
        }
    }
}